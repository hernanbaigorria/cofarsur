jQuery(document).ready(function() {
    // Inicializa todos los inputs con limite maximo de caracteres.
    if ($("input[maxlength]").length && typeof $m.maxlength != 'undefined') {
        $('input[maxlength]').maxlength({
            limitReachedClass: "label label-danger"
        });
    }

    // Inicializa
    start_editors();
    start_date_pickers();
    start_gallery();
    start_masks();
    init_page_collection_sort();
    init_media_collection_sort();
    init_ajax_searchers();
});

// COMPONENTES DE USO GENERAL

    function start_editors() {
        // JS y CSS editor.
        var editors = [];

        // Editor de HTML
        if ($(".rich_editor").length) {
            if (!$.summernote) {
                console.log('No se pueden inicializar editores. Summernote no cargado.');
            }
            else 
            {
                $('.rich_editor').summernote({
                    height: 150,
                    toolbar: [
                        ['style', ['style', 'bold', 'italic', 'underline', 'strikethrough']],
                        ['font', ['fontname']],
                        ['fontsize', ['fontsize']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['insert', ['link', 'picture', 'video', 'hr']],
                        ['view', ['fullscreen', 'codeview']],
                    ],
                    onCreateLink: function(original_link) {
                        return original_link;
                    }
                });
            }
        }

        // Editor de JS
        if ($(".js_editor").length) {
            if (typeof CodeMirror == 'undefined') {
                console.log('No se pueden inicializar editores de javascript. CodeMirror no cargado.');
            }
            else
            {
                $('.js_editor').each(function(e){
                    var editor = CodeMirror.fromTextArea(this, {
                        lineNumbers: true,
                        matchBrackets: true,
                        styleActiveLine: true,
                        theme: "ambiance",
                        mode: 'javascript'
                    });
                    editors.push(editor);
                });
            }
        }

        // Editor de CSS
        if ($(".css_editor").length) {
            if (typeof CodeMirror == 'undefined') {
                console.log('No se pueden inicializar editores de css. CodeMirror no cargado.');
            }
            else {
                $('.css_editor').each(function(e){
                    var editor = CodeMirror.fromTextArea(this, {
                        lineNumbers: true,
                        matchBrackets: true,
                        styleActiveLine: true,
                        theme: "material",
                        mode: 'css'
                    });
                    editors.push(editor);            
                });
            }
        }

        // Cuando hay un editor en un tab, debemos refrescarlo para que se muestre correctamente.
        // Lo refrescamos cada 1 segundo.
        if (editors.length) {
            setInterval(function() {
                $.each(editors, function(index, ed) {
                    ed.refresh();
                })
            }, 1000);
        }
    }

    function start_date_pickers() {
        if ($(".date-picker").length) {
            if (!$.datepicker) {
                console.log('No se pueden inicializar editores. Datepicker no cargado.');
            }
            else {
                $('.date-picker').datepicker({
                    format: 'dd/mm/yyyy',
                    orientation: "left",
                    autoclose: true
                });
            }
        }
    }

    function start_gallery() {
        if ($('#fileupload').length) {
            FormFileUpload.init();
        };

        if ($(".media-gallery .media-item").length >= 12) {
            $(".btn_load_more_media_items").removeClass('hidden');
        };
    }

function start_masks() {
    if (typeof $.minicolors != 'undefined') {
        $('.mask_color').each(function() {
            $(this).minicolors({
                theme: 'bootstrap',
                letterCase: 'uppercase'
            });
        });
    }

    if (typeof $.inputmask != 'undefined') {
        $(".mask_number").inputmask({
            "mask": "9",
            "repeat": 10,
            "greedy": false
        });
        $(".mask_decimal").inputmask('decimal', {
            rightAlign: false,
            radixPoint: ","
        });
        $(".mask_currency").inputmask('$ 999.999.999,99', {
            numericInput: true,
            autounmask: false
        });
    }
}

// reinicializa los ordenadores de colecciones de paginas.
function init_page_collection_sort() {
    if (jQuery().sortable) {
        $('.page_collection_preview').sortable({
            coneHelperSize: true,
            placeholder: 'page_collection_placeholder',
            forcePlaceholderSize: false,
            tolerance: "pointer",
            helper: "clone",
            tolerance: "pointer",
            helper: "clone",
            revert: 250, // animation in milliseconds
            update: function(b, c) {
                update_pages_order();
            }
        });
    }
}

// reinicializa los ordenadores de colecciones de imagenes.
function init_media_collection_sort() {
    if (jQuery().sortable) {
        $('.media_collection_preview').sortable({
            coneHelperSize: true,
            placeholder: 'img_collection_placeholder',
            forcePlaceholderSize: false,
            tolerance: "pointer",
            helper: "clone",
            tolerance: "pointer",
            helper: "clone",
            revert: 250, // animation in milliseconds
            update: function(b, c) {
                update_media_order();
            }
        });
    }
}

function init_ajax_searchers() {
    $('.ajax_search').each(function() {
        var element_id = $(this).attr('id');
        $(this).select2({
            width: "off",
            placeholder: $('#' + element_id).data('placeholder'),
            minimumInputLength: 0,
            quietMillis: 300,
            theme: "bootstrap",
            ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                url: $('#' + element_id).data('url'),
                type: 'post',
                dataType: 'json',
                data: function(params) {
                    return {
                        q: params.term, // search term
                        filter: $('#' + element_id).data('filter')
                    };
                },
                processResults: function(data, page) {
                    // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data
                    return {
                        results: data.ext.feed
                    };
                },
                cache: true
            },
            escapeMarkup: function(markup) {
                return markup;
            },
            templateResult: formatResult,
            templateSelection: formatResultSelection,
        });
    });
}




function update_media_order() {

    $('.media_collection_preview').each(function(i) {
        var this_order = [];
        $(this).find('img').each(function(i) {
            this_order.push($(this).data('id'));
        });
        $(this).parent('.input-group-control').find('input').val(this_order.join(','));
        this_order = null;
    });
}

function update_pages_order() {

    $('.page_collection_preview').each(function(i) {
        var this_order = [];
        $(this).find('span').each(function(i) {
            this_order.push($(this).data('id'));
        });
        $(this).parent('.input-group-control').find('input').val(this_order.join(','));
        this_order = null;
    });
}

// HELPERS VARIOS

var typing_delay = (function() {
    var timer = 0;
    return function(callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();

function parseDate(str) {
    var mdy = str.split('/');
    return new Date(mdy[2], mdy[1], mdy[0]);
}

function date_diff(date1, date2, interval) {
    var second = 1000,
        minute = second * 60,
        hour = minute * 60,
        day = hour * 24,
        week = day * 7;
    date1 = parseDate(date1);
    date2 = parseDate(date2);
    var timediff = date2 - date1;
    if (isNaN(timediff)) return NaN;
    switch (interval) {
        case "years":
            return date2.getFullYear() - date1.getFullYear();
        case "months":
            return (
                (date2.getFullYear() * 12 + date2.getMonth()) -
                (date1.getFullYear() * 12 + date1.getMonth())
            );
        case "weeks":
            return Math.floor(timediff / week);
        case "days":
            return Math.floor(timediff / day);
        case "hours":
            return Math.floor(timediff / hour);
        case "minutes":
            return Math.floor(timediff / minute);
        case "seconds":
            return Math.floor(timediff / second);
        default:
            return undefined;
    }
}

function formatResult(item) {
    var markup = item.text;
    return markup;
}

function formatResultSelection(item) {
    var markup = item.text;
    return markup;
}

// Inputs para slugs
$(document).on('keyup', '.input-slug', function(e) {
    var input_value = $(this).val();
    input_value = input_value.toLowerCase();
    input_value = input_value.replace('á', 'a');
    input_value = input_value.replace('é', 'e');
    input_value = input_value.replace('í', 'i');
    input_value = input_value.replace('ó', 'o');
    input_value = input_value.replace('ú', 'u');
    input_value = input_value.replace('ñ', 'n');
    input_value = input_value.replace(' ', '-');
    input_value = input_value.replace(/[!@#$%^&**(){}\\\/:"<>,.?¿]/gi, '');
    $(this).val(input_value);
});

// HANDLERS VARIOS

// Fuerza el envio de un form, cuando no se puede colocar un button.
$(document).on('click', '.btn-submit', function(e) {
    var form_element = $(this).closest("form")
    form_element.submit();
});

// Fuerza el envio de un form, cuando no se puede colocar un button.
$(document).on('click', '.btn-clean-cache', function(e) {
    send_button('/Ajax/Ajax_pages/clean_cache/', null, function(data) {
    });
    e.preventDefault();
});


// # END HANDLERS VARIOS <<



// PAGINAS

// Genera un modal selector de items en la galeria.
$(document).on('click', '.btn-pick-page', function(e) {
    var element = $(this);
    var cfg_element = $(this).data('cfg');
    var max_items = $(this).data('max');

    var element_original = element.html();
    var params = $(this).data();

    // Agregamos los elementos ya seleccionados.
    var seleccionados = $(this).parent('span').parent('.input-group').find('input').val();
    params.selecteds = seleccionados;

    element.html('<i class="fa fa-spinner fa-spin"></i>');
    send_button('/Ajax/Ajax_pages/get_page_picker/', params, function(data) {
        if (data.cod == 1) {
            $('#modal-page-picker').remove();
            $("body").append(data.ext.composer);
            $('#modal-page-picker').modal("toggle");
            element.html(element_original);
            setTimeout(function() {
                start_page_picker_tree();
                Metronic.initSlimScroll('.scroller');
            }, 500);
        };
    });
    e.preventDefault();
});
// Lleva la seleccion del modal al item de configuracion.
$(document).on('submit', '#frm-page-picker', function(e) {
    var cfg_item = $('#frm-page-picker input[name="cfg_item"]').val();
    var cfg_item_id = "#" + cfg_item;
    var items = $('#pages_tree').jstree().get_top_checked();

    // Colocamos los ids de las imagenes en el input.
    $(cfg_item_id).val(items.join());

    // Actualizamos los previews, mostrando thumbs de las imagenes con id.
    reload_page_previews(cfg_item_id);

    // Cerramos el modal
    $('#modal-page-picker').modal("toggle");

    e.preventDefault();
});

function reload_page_previews(source) {
    var preview_items = $(source).val();
    var lang = $(source).data('lang');
    var source = source;

    var params = {
        source: preview_items,
        lang: lang
    }

    send_button('/Ajax/Ajax_pages/get_previews/', params, function(data) {
        if (data.cod == 1) {
            $(".page_collection_preview[data-source='" + source + "']").html(data.ext.preview_data);
            init_media_collection_sort();
        };
    });
}

function start_page_picker_tree() {
    if ($.jstree) {
        var seleccionados = $('#pages_tree').data('selecteds');
        var params = {
            selecteds: seleccionados
        }

        send_button('/Ajax/Ajax_pages/get_page_picker_feed/', params, function(data) {
            if (data.cod == 1) {
                $('#pages_tree').jstree({
                    'plugins': ["wholerow", "checkbox", "types", "search"],
                    "checkbox": {
                        "three_state": false // Permite la seleccion de todos los nodos, sin selecionar el padre.
                    },
                    "search": {
                        "show_only_matches": true // Permite la seleccion de todos los nodos, sin selecionar el padre.
                    },
                    'core': {
                        "themes": {
                            "responsive": false
                        },
                        'data': data.ext.feed
                    },
                    "types": {
                        "default": {
                            "icon": "fa fa-folder icon-state-warning icon-lg"
                        },
                        "file": {
                            "icon": "fa fa-file icon-state-warning icon-lg"
                        }
                    }
                });
            };
        });
    };
}



var to = false;
$(document).on('keyup', '#page_tree_filter', function(e) {
    if (to) {
        clearTimeout(to);
    }
    to = setTimeout(function() {
        var v = $('#page_tree_filter').val();
        $('#pages_tree').jstree(true).search(v);
    }, 250);
});