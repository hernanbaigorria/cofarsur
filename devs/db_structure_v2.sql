-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 24-02-2018 a las 14:17:16
-- Versión del servidor: 5.7.17-log
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `nwg_hetch`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `collections`
--

CREATE TABLE `collections` (
  `id_collection` int(10) UNSIGNED NOT NULL,
  `collection_type` varchar(45) NOT NULL,
  `creation_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `collection_items`
--

CREATE TABLE `collection_items` (
  `id_item` int(10) UNSIGNED NOT NULL,
  `id_collection` int(10) UNSIGNED NOT NULL,
  `item_value` int(11) NOT NULL,
  `item_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configurations`
--

CREATE TABLE `configurations` (
  `id_config` int(10) UNSIGNED NOT NULL,
  `config_scope` varchar(45) NOT NULL,
  `config_reference` int(11) DEFAULT NULL,
  `config_item` varchar(45) NOT NULL,
  `config_value` text NOT NULL,
  `config_lang` varchar(45) DEFAULT NULL,
  `config_date` datetime DEFAULT NULL,
  `config_last_editor` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `configurations`
--

INSERT INTO `configurations` (`id_config`, `config_scope`, `config_reference`, `config_item`, `config_value`, `config_lang`, `config_date`, `config_last_editor`) VALUES
(1, 'site', 0, 'main_page', '25', NULL, '2017-08-03 10:22:07', 1),
(2, 'site', 0, '404_page', '26', NULL, '2017-08-03 10:22:07', 1),
(3, 'site', 0, 'page_favicon', '27', NULL, '2017-08-03 10:22:07', 1),
(4, 'site', 0, 'logo_header', '28', 'español', '2017-08-03 10:22:07', 1),
(5, 'site', 0, 'page_contact', '29', NULL, '2017-08-03 10:22:07', 1),
(6, 'site', 0, 'page_social_facebook', '30', NULL, '2017-08-03 10:22:08', 1),
(7, 'site', 0, 'page_social_twitter', '31', NULL, '2017-08-03 10:22:08', 1),
(8, 'site', 0, 'page_main_menu_link1', '32', 'español', '2017-08-03 10:22:08', 1),
(9, 'site', 0, 'page_main_menu_opts1', '33', 'español', '2017-08-03 10:22:08', 1),
(10, 'site', 0, 'page_main_menu_link2', '34', 'español', '2017-08-03 10:22:09', 1),
(11, 'site', 0, 'page_main_menu_opts2', '35', 'español', '2017-08-03 10:22:09', 1),
(12, 'site', 0, 'page_main_menu_link3', '36', 'español', '2017-08-03 10:22:09', 1),
(13, 'site', 0, 'page_main_menu_opts3', '37', 'español', '2017-08-03 10:22:09', 1),
(14, 'site', 0, 'page_main_menu_link4', '38', 'español', '2017-08-03 10:22:09', 1),
(15, 'site', 0, 'page_main_menu_opts4', '39', 'español', '2017-08-03 10:22:09', 1),
(16, 'site', 0, 'page_main_menu_link5', '40', 'español', '2017-08-03 10:22:10', 1),
(17, 'site', 0, 'page_main_menu_opts5', '41', 'español', '2017-08-03 10:22:10', 1),
(18, 'site', 0, 'page_main_menu_link6', '42', 'español', '2017-08-03 10:22:10', 1),
(19, 'site', 0, 'page_main_menu_opts6', '43', 'español', '2017-08-03 10:22:10', 1),
(20, 'site', 0, 'page_main_menu_link7', '44', 'español', '2017-08-03 10:22:10', 1),
(21, 'site', 0, 'page_main_menu_opts7', '45', 'español', '2017-08-03 10:22:11', 1),
(22, 'site', 0, 'page_main_menu_link8', '46', 'español', '2017-08-03 10:22:11', 1),
(23, 'site', 0, 'page_main_menu_opts8', '47', 'español', '2017-08-03 10:22:11', 1),
(24, 'site', 0, 'page_foot_menu_p1', '48', NULL, '2017-08-03 10:22:11', 1),
(25, 'site', 0, 'site_title', 'Cafe Ruiz', NULL, '2017-08-03 10:23:38', 1),
(26, 'page', 2, 'page_title', 'Productos', 'español', '2017-08-03 10:40:47', 1),
(27, 'page', 2, 'side_menu_enable', '1', NULL, '2017-08-03 10:40:47', 1),
(28, 'page', 2, 'header_deco_enable', '1', NULL, '2017-08-03 10:40:47', 1),
(29, 'page', 3, 'page_title', 'Historia', 'español', '2017-08-03 10:41:04', 1),
(30, 'page', 3, 'side_menu_enable', '1', NULL, '2017-08-03 10:41:04', 1),
(31, 'page', 3, 'header_deco_enable', '1', NULL, '2017-08-03 10:41:04', 1),
(32, 'page', 4, 'page_title', 'Baristas', 'español', '2017-08-03 10:41:19', 1),
(33, 'page', 4, 'side_menu_enable', '1', NULL, '2017-08-03 10:41:19', 1),
(34, 'page', 4, 'header_deco_enable', '1', NULL, '2017-08-03 10:41:20', 1),
(35, 'page', 5, 'page_title', 'Blog', 'español', '2017-08-03 10:41:34', 1),
(36, 'page', 5, 'side_menu_enable', '1', NULL, '2017-08-03 10:41:34', 1),
(37, 'page', 5, 'header_deco_enable', '1', NULL, '2017-08-03 10:41:34', 1),
(38, 'site', 0, 'page_main_menu', '49', NULL, '2017-08-03 10:42:52', 1),
(39, 'page', 2, 'side_menu', '50', NULL, '2017-08-03 10:49:48', 1),
(40, 'page', 2, 'side_menu_back', '51', NULL, '2017-08-03 10:49:48', 1),
(41, 'page', 3, 'side_menu', '52', NULL, '2017-08-03 10:50:06', 1),
(42, 'page', 3, 'side_menu_back', '53', NULL, '2017-08-03 10:50:06', 1),
(43, 'page', 4, 'side_menu', '54', NULL, '2017-08-03 10:50:40', 1),
(44, 'page', 4, 'side_menu_back', '55', NULL, '2017-08-03 10:50:40', 1),
(45, 'site', 0, 'numberos_contacto', '0354-1556577 / 0351-1558866', NULL, '2017-08-03 11:28:19', 1),
(46, 'site', 0, 'correo', 'consultas@caferuiz.com.ar', NULL, '2017-08-03 11:28:19', 1),
(50, 'component', 1, 'img_slider', '56', NULL, '2017-08-03 12:19:48', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `media`
--

CREATE TABLE `media` (
  `id_media` int(10) UNSIGNED NOT NULL,
  `media_type` varchar(45) DEFAULT NULL,
  `media_name` varchar(100) DEFAULT NULL,
  `media_file_name` varchar(100) DEFAULT NULL,
  `media_file_size` int(11) DEFAULT NULL,
  `upload_date` datetime DEFAULT NULL,
  `media_group` int(11) NOT NULL DEFAULT '0',
  `media_uploader` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `media_groups`
--

CREATE TABLE `media_groups` (
  `id_group` int(11) NOT NULL,
  `group_name` varchar(50) NOT NULL,
  `id_user` int(11) NOT NULL,
  `creation_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pages`
--

CREATE TABLE `pages` (
  `id_page` int(10) UNSIGNED NOT NULL,
  `is_section` tinyint(1) NOT NULL DEFAULT '0',
  `is_external` tinyint(1) NOT NULL DEFAULT '0',
  `id_parent_page` int(11) NOT NULL DEFAULT '0',
  `page_title` varchar(100) NOT NULL,
  `page_slug` varchar(100) NOT NULL,
  `page_order` int(11) NOT NULL DEFAULT '0',
  `th_layout` varchar(45) DEFAULT NULL,
  `page_cache_time` smallint(6) NOT NULL DEFAULT '0',
  `page_custom_css` longtext,
  `page_custom_js` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `page_components`
--

CREATE TABLE `page_components` (
  `id_component` int(11) NOT NULL,
  `id_page` int(11) NOT NULL,
  `page_version` int(11) NOT NULL DEFAULT '1',
  `id_parent_component` int(11) NOT NULL DEFAULT '0',
  `th_component` varchar(45) NOT NULL,
  `component_order` int(11) NOT NULL DEFAULT '0',
  `component_creation_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `page_components_content`
--

CREATE TABLE `page_components_content` (
  `id_content` int(11) NOT NULL,
  `id_component` int(11) NOT NULL,
  `content_name` varchar(45) NOT NULL,
  `content_value` longtext NOT NULL,
  `content_lang` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `page_versions`
--

CREATE TABLE `page_versions` (
  `id_page_version` int(10) UNSIGNED NOT NULL,
  `id_page` int(10) UNSIGNED NOT NULL,
  `version_number` int(11) NOT NULL,
  `version_comment` text,
  `version_last_modification` datetime NOT NULL,
  `version_last_modifier` int(10) UNSIGNED NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `date_creation` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id_permission` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `permission_group` varchar(45) NOT NULL,
  `permission_item` varchar(45) NOT NULL,
  `permission_value` tinyint(1) NOT NULL,
  `permission_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `permissions`
--

INSERT INTO `permissions` (`id_permission`, `id_user`, `permission_group`, `permission_item`, `permission_value`, `permission_date`) VALUES
(1, 1, 'gestorp', 'manage_pages', 1, '2017-08-01 12:31:40'),
(2, 1, 'gestorp', 'manage_menus', 1, '2017-08-01 12:31:40'),
(3, 1, 'gestorp', 'manage_modules', 1, '2017-08-01 12:31:40'),
(4, 1, 'gestorp', 'manage_media', 1, '2017-08-01 12:31:40'),
(5, 1, 'gestorp', 'manage_configurations', 1, '2017-08-01 12:31:40'),
(6, 1, 'gestorp', 'manage_users', 1, '2017-08-01 12:31:40'),
(7, 1, 'gestorp', 'manage_create_pages', 1, '2017-08-01 12:33:02'),
(8, 1, 'gestorp', 'upload_files', 1, '2017-08-01 12:42:17'),
(9, 1, 'gestorp', 'manage_permissions', 1, '2017-08-01 12:42:54'),
(10, 1, 'pages', '1', 1, '2017-08-03 10:09:03'),
(11, 1, 'pages', '2', 1, '2017-08-03 10:40:47'),
(12, 1, 'pages', '3', 1, '2017-08-03 10:41:05'),
(13, 1, 'pages', '4', 1, '2017-08-03 10:41:20'),
(14, 1, 'pages', '5', 1, '2017-08-03 10:41:34'),
(15, 1, 'gestorp', 'remove_files', 0, '2017-08-03 12:39:35'),
(16, 1, 'gestorp', 'edit_files', 0, '2017-08-03 12:39:35'),
(17, 1, 'gestorp', 'replace_files', 0, '2017-08-03 12:39:35'),
(18, 1, 'modules', 'blog', 1, '2017-08-04 17:46:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `user_name` varchar(45) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_picture` varchar(45) DEFAULT NULL,
  `user_password` varchar(45) NOT NULL,
  `user_language` varchar(45) DEFAULT 'spanish',
  `user_password_restore` varchar(45) DEFAULT NULL,
  `user_registration_date` datetime DEFAULT NULL,
  `user_last_login` datetime DEFAULT NULL,
  `user_login_count` int(11) NOT NULL DEFAULT '0',
  `user_deletion_date` datetime DEFAULT NULL,
  `user_dev` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id_user`, `user_name`, `user_email`, `user_picture`, `user_password`, `user_language`, `user_password_restore`, `user_registration_date`, `user_last_login`, `user_login_count`, `user_deletion_date`, `user_dev`) VALUES
(1, 'hernanbaigorria', 'hbaigorria@inglobe.com.ar', NULL, '202cb962ac59075b964b07152d234b70', 'spanish', NULL, '2017-08-01 00:00:00', '2018-02-24 11:16:14', 7, NULL, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `collections`
--
ALTER TABLE `collections`
  ADD PRIMARY KEY (`id_collection`);

--
-- Indices de la tabla `collection_items`
--
ALTER TABLE `collection_items`
  ADD PRIMARY KEY (`id_item`),
  ADD KEY `fk_collection_items_collections1_idx` (`id_collection`);

--
-- Indices de la tabla `configurations`
--
ALTER TABLE `configurations`
  ADD PRIMARY KEY (`id_config`),
  ADD KEY `CFG_SCOPE` (`config_scope`),
  ADD KEY `CFG_REF` (`config_reference`),
  ADD KEY `CFG_ITEM` (`config_item`);

--
-- Indices de la tabla `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id_media`);

--
-- Indices de la tabla `media_groups`
--
ALTER TABLE `media_groups`
  ADD PRIMARY KEY (`id_group`);

--
-- Indices de la tabla `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id_page`),
  ADD KEY `PAG_SLUG` (`page_slug`);

--
-- Indices de la tabla `page_components`
--
ALTER TABLE `page_components`
  ADD PRIMARY KEY (`id_component`),
  ADD KEY `CMP_PAGE` (`id_page`),
  ADD KEY `CMP_NAME` (`th_component`);

--
-- Indices de la tabla `page_components_content`
--
ALTER TABLE `page_components_content`
  ADD PRIMARY KEY (`id_content`),
  ADD KEY `fk_fields_components1_idx` (`id_component`),
  ADD KEY `CNT_NAME` (`content_name`);

--
-- Indices de la tabla `page_versions`
--
ALTER TABLE `page_versions`
  ADD PRIMARY KEY (`id_page_version`),
  ADD KEY `fk_page_versions_pages1_idx` (`id_page`),
  ADD KEY `VER_NUMBER` (`version_number`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id_permission`),
  ADD KEY `fk_permissions_users1_idx` (`id_user`),
  ADD KEY `PER_GROUP` (`permission_group`),
  ADD KEY `PER_ITEM` (`permission_item`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `USR_EMAIL` (`user_email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `collections`
--
ALTER TABLE `collections`
  MODIFY `id_collection` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `collection_items`
--
ALTER TABLE `collection_items`
  MODIFY `id_item` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `configurations`
--
ALTER TABLE `configurations`
  MODIFY `id_config` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT de la tabla `media`
--
ALTER TABLE `media`
  MODIFY `id_media` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `media_groups`
--
ALTER TABLE `media_groups`
  MODIFY `id_group` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pages`
--
ALTER TABLE `pages`
  MODIFY `id_page` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `page_components`
--
ALTER TABLE `page_components`
  MODIFY `id_component` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `page_components_content`
--
ALTER TABLE `page_components_content`
  MODIFY `id_content` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `page_versions`
--
ALTER TABLE `page_versions`
  MODIFY `id_page_version` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id_permission` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `collection_items`
--
ALTER TABLE `collection_items`
  ADD CONSTRAINT `fk_collection_items_collections1` FOREIGN KEY (`id_collection`) REFERENCES `collections` (`id_collection`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `page_components_content`
--
ALTER TABLE `page_components_content`
  ADD CONSTRAINT `fk_fields_components1` FOREIGN KEY (`id_component`) REFERENCES `page_components` (`id_component`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `page_versions`
--
ALTER TABLE `page_versions`
  ADD CONSTRAINT `fk_page_versions_pages1` FOREIGN KEY (`id_page`) REFERENCES `pages` (`id_page`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD CONSTRAINT `fk_permissions_users1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
