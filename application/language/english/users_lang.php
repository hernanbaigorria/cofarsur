<?php 

$lang['users_users'] 					= 'Users';
$lang['users_user'] 					= 'User';
$lang['users_edit_user'] 				= 'Edit user';
$lang['users_create_user'] 				= 'Create new user';
$lang['users_avatar'] 					= 'Avatar';
$lang['users_registration_date']		= 'Registration Date';
$lang['users_last_login']				= 'Last Access';
$lang['users_login_count']				= 'Login Count';
$lang['users_new_user'] 				= 'New User';
$lang['users_name'] 					= 'Name';
$lang['users_email'] 					= 'Email';
$lang['users_current_password']			= 'Current Password';
$lang['users_new_password']				= 'New Password';
$lang['users_r_new_password']			= 'Re-type New Password';
$lang['users_language']					= 'Language';
$lang['users_permissions']				= 'Permissions';


$lang['users_update_profile_picture']	= 'Update profile picture';
$lang['users_change_picture']			= 'Change Picture';
$lang['users_pick_another']				= 'Pick Another';
$lang['users_upload_picture']			= 'Upload Picture';


// Mensajes de notificaicon
$lang['users_created_succesfully'] 		= 'User created successfully';
$lang['users_removed_succesfully'] 		= 'User removed successfully';
$lang['users_delete_sure'] 				= 'Are you sure?';
