<?php 

$lang['login_admin_access']				= 'Administrator Login';
$lang['login_password'] 				= 'Password';
$lang['login_continue'] 				= 'Continue';
$lang['login_restore_password']			= '¿Forget Password?';
$lang['login_write_email_password']		= 'Write your email and password';
$lang['login_write_email_password']		= 'Write your email and password';
$lang['login_forget_password']			= 'Forget Password ?';
$lang['login_enter_email_address']		= 'Enter your e-mail address below to recive a temporal password.';



// Mensajes de notificaicon
$lang['login_incorrect_email_or_p']		= 'Wrong Email or Password';
$lang['login_user_no_exist'] 			= 'Aparently, you are not registered.';
$lang['login_temp_password_problem']	= 'An error occurred while generate your temporary password.';
$lang['login_temp_password_ok'] 		= 'Done, a temporary password was sent.';
