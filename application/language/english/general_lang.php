<?php 

// Traducciones de rotulos de la aplicacion.

	// Traducciones de Menus y titulos de pantallas...
		$lang['general_dashboard'] 				= 'Dashboard';
		$lang['general_pages'] 					= 'Pages';
		$lang['general_menus'] 					= 'Menus';
		$lang['general_media'] 					= 'Multimedia';
		$lang['general_modules'] 				= 'Modules';
		$lang['general_configuration']			= 'Configuration';
		$lang['general_system']					= 'System Configuration';
		$lang['general_users'] 					= 'Users';
		$lang['general_account_configuration'] 	= 'Account Settings';
		$lang['general_change_password'] 		= 'Change Password';
		$lang['general_account_login'] 			= 'Log In';
		$lang['general_account_logout'] 		= 'Log Out';

	// Traducciones de botones y rotulos 
		$lang['general_new'] 					= 'New';
		$lang['general_edit'] 					= 'Edit';
		$lang['general_quick_edit'] 			= 'Quick Edit';
		$lang['general_live_edit'] 				= 'Live Edit';
		$lang['general_config'] 				= 'Config';
		$lang['general_save'] 					= 'Save';
		$lang['general_cancel'] 				= 'Cancel';
		$lang['general_back'] 					= 'Back';
		$lang['general_close'] 					= 'Close';
		$lang['general_delete'] 				= 'Delete';
		$lang['general_done'] 					= 'Done';
		$lang['general_discard'] 				= 'Discard';
		$lang['general_write_here'] 			= 'Write Here ...';
		$lang['general_nothing_selected'] 		= 'Nothing Selected ...';
		$lang['general_allow_translations'] 	= 'Can be translated';
		$lang['general_load_more'] 				= 'Load more ...';
		$lang['general_select_images'] 			= 'Select Images';
		$lang['general_select_image'] 			= 'Select Image';
		$lang['general_select_videos'] 			= 'Select Videos';
		$lang['general_pick_images'] 			= 'Pick some images from gallery';
		$lang['general_select_pages'] 			= 'Select Pages';
		$lang['general_select_documents']		= 'Select Documents';
		$lang['general_pick_page'] 				= 'Pick some pages';
		$lang['general_pages_selected'] 		= 'Pages selected';
		$lang['general_currently_editing']		= 'Editing';
		$lang['general_switch_to']				= 'Switch to';
		$lang['general_preview']				= 'Preview';
		$lang['general_new_draft']				= 'New Draft';
		$lang['general_home_page']				= 'Home Page';
		$lang['general_force_size']				= 'Force Size';
		$lang['general_width']					= 'Width';
		$lang['general_fit_to_screen']			= 'Fit to Screen';
		$lang['general_delete_sure'] 			= 'Are you sure?';
		$lang['general_filter'] 				= 'Filter';
		$lang['general_search'] 				= 'Search';
		$lang['general_creation_date'] 			= 'Creation Date';

	// Traducciones de Mensajes de notificacion
		$lang['general_msg_saved_ok'] 			= 'Your configuration has been successfully saved.';
		$lang['general_no_permission'] 			= 'Your account doesn`t have permissions to do this.';
		$lang['general_error_ocurred'] 			= 'An error occurred while processing your request.';
		$lang['general_pwn_match_error'] 		= 'Passwords doesn`t match.';
		$lang['general_email_used'] 			= 'Email already in use';
		$lang['general_configurations_updated']	= 'Configurations has been updated.';
		$lang['general_everything_ok']			= 'Everything OK.';
		$lang['general_ns_must_login_again']	= 'Nothing saved. Must log in again.';
		$lang['general_must_login_again']		= 'Must log in again.';
		$lang['general_error_file_exist']		= 'Filename already exist.';

// Traducciones Generales de contenido fijo de la aplicacion.
	
	// Dashboard
		$lang['general_hello'] 					= 'Hi';

