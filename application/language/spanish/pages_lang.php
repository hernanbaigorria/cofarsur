<?php 

$lang['pages_pages'] 					= 'Páginas';
$lang['pages_pages_simples'] 			= 'Páginas Simples';
$lang['pages_pages_grouped'] 			= 'Págnas Agrupadas';
$lang['pages_pages_external'] 			= 'Links';
$lang['pages_subpages'] 				= 'Sub-Páginas';
$lang['pages_page'] 					= 'Página';
$lang['pages_sections'] 				= 'Secciones';
$lang['pages_section'] 					= 'Sección';
$lang['pages_section_page'] 			= 'Pagina para una sección...';
$lang['pages_external_pages'] 			= 'Links a paginas externas';
$lang['pages_edit_page'] 				= 'Editar página';
$lang['pages_edit_version'] 			= 'Editar versión';
$lang['pages_create_page'] 				= 'Crear una página';
$lang['pages_create_subpage'] 			= 'Crear una sub-página';
$lang['pages_delete_section'] 			= 'Eliminar esta sección';
$lang['pages_new_page'] 				= 'Página nueva';
$lang['pages_back'] 					= 'Volver a páginas';
$lang['pages_page_basics'] 				= 'Configuración';
$lang['pages_page_content'] 			= 'Contenido de página';
$lang['pages_page_versions'] 			= 'Versiones';
$lang['pages_page_settings'] 			= 'Personalizar';
$lang['pages_page_name'] 				= 'Nombre interno';
$lang['pages_page_slug'] 				= 'URL pública';
$lang['pages_page_parent_section']		= 'Seccion a la que pertenece';
$lang['pages_page_layout'] 				= 'Diseño';
$lang['pages_page_version'] 			= 'Versión';
$lang['pages_page_new_version'] 		= 'Nueva Versión';
$lang['pages_page_verion_creation_date']= 'Creada';
$lang['pages_page_version_comment']		= 'Comentario';
$lang['pages_spage_uri'] 				= '/subpages-url';
$lang['pages_wich_page_type'] 			= '¿Que tipo de página quieres crear?';
$lang['pages_descr_pages']	 			= 'Una pagina aislada, que no pertenece a ninguna seccion. Por ejemplo una página de contacto, una landing page, la home ...';
$lang['pages_descr_section']			= 'Es una página que puede tener otras paginas en su interior';
$lang['pages_descr_section_pages'] 		= 'Es una página que pertenece a una seccion, y hereda el diseño y las configuraciones de esta';
$lang['pages_descr_external'] 			= 'Es un link que apunta a un sitio externo a este';
$lang['pages_create_from'] 				= 'Crear una nueva versión a partir de: ';

$lang['pages_status'] 					= 'Estado';
$lang['pages_status_online'] 			= 'Publicado';
$lang['pages_status_draft'] 			= 'Borrador';
$lang['pages_status_to_update']			= 'Cambios no publicados';
$lang['pages_status_published']			= 'Publicada';

$lang['pages_add_block'] 				= 'Agregar un bloque';
$lang['pages_add_module'] 				= 'Agregar un módulo';
$lang['pages_page_configuration']		= 'Opciones de página';
$lang['pages_block_configuration']		= 'Configuración del bloque';

$lang['pages_page_cache']				= 'Cache';
$lang['pages_page_cache_disabled']		= 'Desactivado';
$lang['pages_page_cache_site']			= 'Definido por el sitio';
$lang['pages_page_cache_minutes']		= 'Minutos';
$lang['pages_page_cache_days']			= 'Dias';
$lang['pages_page_cache_hours']			= 'Horas';
$lang['pages_page_custom']				= 'CSS/JS';
$lang['pages_page_custom_css']			= 'CSS';
$lang['pages_page_custom_js']			= 'JS';

$lang['pages_no_pages']					= 'No hay paginas para mostrar';
$lang['pages_no_sub_pages']				= 'No hay paginas hijas para mostrar.';
$lang['pages_discard_version']			= 'Descartar';
$lang['pages_publish_version']			= 'Publicar';
$lang['pages_publish_version_editor']	= 'Esta versión no esta publicada. Click aquí para publicar.';
$lang['pages_empty_version']			= 'Versión vacia.';
$lang['pages_editing_live']				= 'Atención: Actualmente estas editando la version publicada. Los cambios se reflejan en vivo.';


// Mensajes de notificaicon
$lang['pages_error_no_layout'] 			= 'Debes seleccionar un diseño';
$lang['pages_error_short_slug'] 		= 'La URL es muy corta';
$lang['pages_error_short_title'] 		= 'El título es muy corto';
$lang['pages_error_no_parent'] 			= 'Debes seleccionar una seccion padre.';
$lang['pages_error_uri_used'] 			= 'La URL indicada ya esta en uso.';

$lang['pages_created_succesfully'] 		= 'Página creada correctamnete';
$lang['pages_removed_succesfully'] 		= 'Página eliminada correctamente';
$lang['pages_v_created_succesfully'] 	= 'Versión creada correctamente';
$lang['pages_v_published_succesfully'] 	= 'Versión publicada correctamente';
$lang['pages_v_removed_succesfully'] 	= 'Versión eliminada correctamente';
$lang['pages_delete_sure'] 				= '¿Estas seguro?';
$lang['pages_publish_version_sure'] 	= '¿Estas seguro?';
$lang['pages_remove_version_sure'] 		= '¿Estas seguro?';
