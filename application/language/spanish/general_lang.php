<?php 

// Traducciones de rotulos de la aplicacion.

	// Traducciones de Menus y titulos de pantallas...
		$lang['general_dashboard'] 				= 'Inicio';
		$lang['general_pages'] 					= 'Paginas';
		$lang['general_menus'] 					= 'Menus';
		$lang['general_media'] 					= 'Multimedia';
		$lang['general_modules'] 				= 'Módulos';
		$lang['general_configuration']			= 'Configuraciones';
		$lang['general_users'] 					= 'Usuarios';
		$lang['general_account_configuration'] 	= 'Config. de Cuenta';
		$lang['general_system']					= 'System Configuration';
		$lang['general_change_password'] 		= 'Cambiar Contraseña';
		$lang['general_account_login'] 			= 'Entrar';
		$lang['general_account_logout'] 		= 'Salir';

	// Traducciones de botones y rotulos 
		$lang['general_new'] 					= 'Nuevo';
		$lang['general_edit'] 					= 'Editar';
		$lang['general_quick_edit'] 			= 'Edit rápido';
		$lang['general_live_edit'] 				= 'Editar en vivo';
		$lang['general_config'] 				= 'Config';
		$lang['general_save'] 					= 'Guardar';
		$lang['general_cancel'] 				= 'Cancelar';
		$lang['general_back'] 					= 'Volver';
		$lang['general_close'] 					= 'Cerrar';
		$lang['general_delete'] 				= 'Borrar';
		$lang['general_done'] 					= 'Listo';
		$lang['general_discard'] 				= 'Descartar';
		$lang['general_write_here'] 			= 'Escriba Aquí ...';
		$lang['general_nothing_selected'] 		= 'No se selecciono nada ...';
		$lang['general_allow_translations'] 	= 'No puede ser traducido';
		$lang['general_load_more'] 				= 'Cargar mas ...';
		$lang['general_select_images'] 			= 'Seleccionar imagenes';
		$lang['general_select_image'] 			= 'Seleccionar imagen';
		$lang['general_select_videos'] 			= 'Seleccionar videos';
		$lang['general_pick_images'] 			= 'Seleccionar imagenes de la galería';
		$lang['general_select_pages'] 			= 'Seleccionar páginas';
		$lang['general_select_documents']		= 'Seleccionar Documentos';
		$lang['general_pick_page'] 				= 'Seleccionar algunas páginas';
		$lang['general_pages_selected'] 		= 'Pagina seleccionada';
		$lang['general_currently_editing']		= 'Editando';
		$lang['general_switch_to']				= 'Cambiar a';
		$lang['general_preview']				= 'Vista previa';
		$lang['general_new_draft']				= 'Crear borrador';
		$lang['general_home_page']				= 'Pagina de inicio';
		$lang['general_force_size']				= 'Forzar tamaño';
		$lang['general_width']					= 'Ancho';
		$lang['general_fit_to_screen']			= 'Ajustar a la pantalla';
		$lang['general_delete_sure'] 			= '¿Estas seguro?';
		$lang['general_filter'] 				= 'Filtrar';
		$lang['general_search'] 				= 'Buscar';
		$lang['general_creation_date'] 			= 'Fecha de creación';

	// Traducciones de Mensajes de notificacion
		$lang['general_msg_saved_ok'] 			= 'Los cambios se guardaron correctamente';
		$lang['general_no_permission'] 			= 'Tu usuario no tiene permisos para realizar esto';
		$lang['general_error_ocurred'] 			= 'Ocurrio un error cuando procesabamos tu solicitud';
		$lang['general_pwn_match_error'] 		= 'Las contraseñas no coinciden';
		$lang['general_email_used'] 			= 'Ese email ya esta en uso';
		$lang['general_configurations_updated']	= 'Se actualizó la configuación';
		$lang['general_everything_ok']			= 'Todo ok.';
		$lang['general_ns_must_login_again']	= 'No se guardo nada. Debe iniciar sesión nuevamente.';
		$lang['general_must_login_again']		= 'Debe iniciar sesión nuevamente';
		$lang['general_error_file_exist']		= 'Ese nombre de achivo ya existe.';

// Traducciones Generales de contenido fijo de la aplicacion.
	
	// Dashboard
		$lang['general_hello'] 					= 'Hola';

