<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * NawGlobe common helpers.
 */

	// Return an array with pages that match specified term.
	if ( ! function_exists('search_pages'))
	{
		function search_pages($term = NULL, $page = 0, $items_per_page = 20) 
		{
			$CI =& get_instance();

			$pages = $CI->pages->search($term, $page, $items_per_page);

			return $pages;
		}
	}

	// Return avaiable modules.
	if ( ! function_exists('get_modules'))
	{
		function get_modules($list_all = FALSE) 
		{
	        $MODULES_PATH   = PATH_THEME_CODE.'modules';

	        // Each folder should be a module.
	        $modules = glob($MODULES_PATH . '/*' , GLOB_ONLYDIR);

	        // Clean GLOB full path, in order to get folder name ~ module name.
	        // And check permission
	        foreach ($modules as $order => $module) 
	        {
	            $module         = explode('/', $module);
	            if (isset($module[1]))
	                $modules[$order]  = $module[count($module) - 1];

	            // Check if user has permission to view this module.
		        if ($list_all == FALSE)
		            if (!check_permissions('modules', $modules[$order])) 
		            	unset($modules[$order]);
	        }

	        return (is_array($modules)) ? $modules : array();
		}
	}

	// Return child pages fo specified page.
	if ( ! function_exists('page_childs'))
	{
		function page_childs($id_page = FALSE) 
		{
			$CI =& get_instance();
			if ($id_page == FALSE) return array();
			
			// Revisamos que no se proporcione accidentalmente una lista de paginas.
			$page_list = explode(',', $id_page);
			$id_page = (count($page_list) > 0) ? $page_list[0] : $id_page;

			$pages = $CI->pages->get_pages('id_parent_page', $id_page, 0, 500);

			return $pages;
		}
	}

	// Retorna la URL de una pagina.
	if ( ! function_exists('page_uri'))
	{
		function page_uri($id_page = FALSE, $disabled = FALSE) 
		{
			if ($disabled)
				return 'javascript:void();';

			$CI =& get_instance();
			if ($id_page 		== FALSE)	return 'javascript:void();';
			
			// Revisamos que no se proporcione accidentalmente una lista de paginas.
			$page_list = explode(',', $id_page);
			$id_page = (count($page_list) > 0) ? $page_list[0] : $id_page;

			$page = $CI->pages->get_page($id_page);

			if ($page !== FALSE) 
			{
				if ($page['id_parent_page'] != 0)
				{
					$parent_page = $CI->pages->get_page($page['id_parent_page']);
					return base_url().$parent_page['page_slug'].'/'.$page['page_slug'];
				}
				if (!$page['is_external']) 
				{
					return base_url().$page['page_slug'];
				}
				return $page['page_slug'];
			}

			// Devolvemos un link nulo en caso de error.
			return 'javascript:void();';
		}
	}

	// Retorna el titulo de una pagina, en el idioma especificado. Si es que la pagina tiene una confin page_title guardada. 
	// De lo contrario retorna el nombre interno.
	if ( ! function_exists('page_title'))
	{
		function page_title($id_page, $lang = FALSE) 
		{
			$CI =& get_instance();
			if ($id_page 		== FALSE)	return FALSE;
			if ($lang 	 		== FALSE) 	$lang = get_web_lang();

			$page = $CI->pages->get_page($id_page);

			if ($page !== FALSE) 
			{
				// vemos si la pagina tiene una configuracion guardada, que nos permita obtener  el titulo en el idioma requerido.
				$page_configs = $CI->configurations->get_configurations('page', $id_page, $lang);
				if (isset($page_configs['page_title']))
					return (strlen($page_configs['page_title']) > 0) ? $page_configs['page_title']  : $page['page_title'];

				return $page['page_title'];
			}

			return NULL;
		}
	}

	// Retorna la URL de un archivo multimedia
	if ( ! function_exists('media_uri'))
	{
		function media_uri($id_media, $thumb = FALSE, $short = FALSE) 
		{
			// Obtenemos la estructura de configuracion.
			$CI 	=& get_instance();
			$media 	= $CI->media->get_file($id_media);

			if ($media != FALSE)
			{
				if (strpos($media['media_type'], 'video') !== FALSE)
					if ($thumb == FALSE)
						return UPLOADS_URL.$media['media_file_name'];
					else
						return static_url('global/img/video.ico');
				else 
				{
					if (!$short) {
						if ($thumb == FALSE)
							return UPLOADS_URL.$media['media_file_name'];
						else
							return THUMBNAILS_URL.$media['media_file_name'];
					}
					else
						return base_url().'/M/'.$media['id_media'];
				}
			}
			else
				return NULL;
		}
	}

	// Retorna la URL de un archivo multimedia
	if ( ! function_exists('media_title'))
	{
		function media_title($id_media) 
		{
			// Obtenemos la estructura de configuracion.
			$CI 	=& get_instance();
			$media 	= $CI->media->get_file($id_media);

			if ($media != FALSE)
				return (!empty($media['media_name'])) ? $media['media_name'] : $media['media_file_name'];
			else
				return 'Image not found.';
		}
	}

	// Se utiliza para interpretar los espacios editarbles dentro de un bloque
	if ( ! function_exists('gtp_paragraph'))
	{
		function gtp_paragraph($name, $tag, $id_component, $lang = FALSE, $editable = FALSE) 
		{
			$CI =& get_instance();
			if ($lang 	 		== FALSE) 	$lang = get_web_lang();
			if ($id_component 	== FALSE)	return FALSE;
			
			$CI->contents->render_content($name, $tag, $id_component, $lang, $editable, 'text');
		}
	}

	if ( ! function_exists('gtp_get_content'))
	{
		function gtp_get_content($name, $id_component, $lang = FALSE) 
		{
			$CI =& get_instance();
			if ($lang 	 		== FALSE) 	$lang = get_web_lang();
			if ($id_component 	== FALSE)	return FALSE;
			
			return $CI->contents->return_content($name, $id_component, $lang);
		}
	}
	// Se utiliza para interpretar los espacios editarbles dentro de un bloque
	if ( ! function_exists('gtp_html'))
	{
		function gtp_html($name, $tag, $id_component, $lang = FALSE, $editable = FALSE) 
		{
			$CI =& get_instance();
			if ($lang 	 		== FALSE) 	$lang = get_web_lang();
			if ($id_component 	== FALSE)	return FALSE;
			
			$CI->contents->render_content($name, $tag, $id_component, $lang, $editable, 'html');
		}
	}

	// Renderiza una pagina de modulo en una pagina contenedora.
	if ( ! function_exists('render_module'))
	{
		function render_module($module_name, $data = array(), $lang = FALSE, $editable = FALSE, $return = TRUE) 
		{
			$CI =& get_instance();

			// Cargamos la configuración de sistema.
			$system = $CI->configurations->get_configurations('system', 0, get_web_lang());
			
			// Si hay restricción de dominio para acceso publico.
			if (!empty($system['allow_domain'])) {
			    $allowed_domains = explode(',', $system['allow_domain']);
			    if (!in_array($_SERVER['HTTP_HOST'], $allowed_domains)) 
			    {
			    	header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
			    	exit();
			    }
			}

			// Identificamos a que pagina pertenece.
	        $page   = $CI->pages->get_pages('page_slug', $module_name);
	        $page = (isset($page[0])) ? $page[0] : FALSE;

	        if ($page == FALSE)     return FALSE;               // Si la pagina no existe, devolvemos FALSE.
	        if ($lang == FALSE)     $lang = get_web_lang();     // Si no especificaron lenguaje, mostramos el lenguaje web seleccionado.

	        $site_cfg = $CI->configurations->get_configurations('site', 0, $lang);
	        $page_cfg = $CI->configurations->get_configurations('page', $page['id_page'], $lang);
	        
			$site_cache_time 		= (!empty($system['cache_time'])) 			? $system['cache_time'] 		: 0;
			$site_enable_profiler	= (!empty($system['enable_profiler'])) 		? $system['enable_profiler'] 	: FALSE;
			$site_enable_minify		= (!empty($system['enable_html_minify'])) 	? $system['enable_html_minify'] : FALSE;

	        $data['PAGE_CFG']   = cfg_merge($site_cfg, $page_cfg);
	        $data['LANGS']      = $CI->config->item('available_languages');
	        $data['page']       = $page;

	        // Si no hay version, devolvemos false.
	            if ($data['page']['published_version'] == FALSE)
	                return FALSE;

	        // Verificamos que los datos provistos, contengan la información minima.
	            if (!isset($data['page']) OR !isset($data['PAGE_CFG'])) {
	                show_error("Layouts: No se especifico pagina o configuracion, a mostrar en layout: ".$layout_name);
	            }

	        $layout  = $page['th_layout'];

	        $data['VERSION'] = $data['page']['published_version'];
	        $data['ID_PAGE'] = $page['id_page'];

	        $_HTML =  $CI->layouts->render_layout($layout, $data, $lang, $editable, $return);   

			if ($site_enable_profiler == TRUE) {
				$CI->output->enable_profiler(TRUE);
			}

			if ($site_enable_minify == TRUE) {
	    		$_HTML = minify_html($_HTML);
			}

			$CI->output->set_output($_HTML);

			if (!empty($_HTML)) 
			{
				$page_cache_time = $page['page_cache_time'];

				if ($page_cache_time > 0) {
					$CI->output->cache($page_cache_time);
				} 
				if ($page_cache_time == 0) {
					if ($site_cache_time > 0) {
						$CI->output->cache($site_cache_time);
					}
				} 
			}
		}
	}

	// Renderiza una pagina completa.
	if ( ! function_exists('render_page'))
	{
		function render_page($id_page, $lang = FALSE, $version = FALSE, $editable =  FALSE, $return = FALSE) 
		{
			$CI =& get_instance();
			if ($lang == FALSE) 	$lang 		= get_web_lang();
			if ($version == FALSE)	$version 	= $CI->pages->get_published_version($id_page) ;
			return $CI->pages->render_page($id_page, $lang, $version, $editable, $return);
		}
	}

	// Renderiza componentes de la pagina solicitada.
	if ( ! function_exists('render_components'))
	{
		function render_components($id_page, $lang, $version, $editable = FALSE) 
		{
			$CI =& get_instance();

			// Obtenemos la estructura de configuracion.
			$page['id_page'] 	  = $id_page;
			$page['page_version'] = $version;
			$hlp_components = $CI->components->get_components($page, FALSE, 0, 500);

			foreach ($hlp_components as $key => $hlp_component) 
				$CI->components->render_component($hlp_component['id_component'], $lang, $version, $editable);
		}
	}

/**
 * Helpers varios para cuestiones generales, de configuracion, permisos y otros.
 */

	// Retorna el lenguaje establecido por el usuario visitante.
	if ( ! function_exists('get_web_lang'))
	{
		function get_web_lang() 
		{
			$CI =& get_instance();

			$available_languages = $CI->config->item('available_languages');
			
			if (in_array($CI->input->cookie('web_lang', TRUE), $available_languages))
				$current_lang = $CI->input->cookie('web_lang', TRUE);
			else
				$current_lang = $available_languages[0];

			$cookie = array(
			    'name'   => 'web_lang',
			    'value'  => $current_lang,
			    'expire' => '86500'
			);

			$CI->input->set_cookie($cookie);

			return $current_lang;
		}
	}	

	// Cambia el idioma de presentacion de la web al publico.
	if ( ! function_exists('set_web_lang'))
	{
		function set_web_lang($lang) 
		{
			$CI =& get_instance();

			$available_languages = $CI->config->item('available_languages');
			
			if (in_array($lang, $available_languages))
				$current_lang = $lang;
			else
				$current_lang = $available_languages[0];

			$cookie = array(
			    'name'   => 'web_lang',
			    'value'  => $lang,
			    'expire' => '86500'
			);

			$CI->input->set_cookie($cookie);

			return $current_lang;
		}
	}

	// Cambia el idioma de presentacion de la web al publico.
	if ( ! function_exists('goto_lang'))
	{
		function goto_lang($lang) 
		{
			echo base_url().URL_SWITCH_LANG.$lang;
		}
	}

	// Combina dos estructuras de configuraction, priorizando la segunda.
	if ( ! function_exists('cfg_merge'))
	{
		function cfg_merge($top_cfg, $override_cfg) 
		{
			foreach ($override_cfg as $key => $cfg)
				if (!empty($cfg) OR !isset($top_cfg[$key])) 
					$top_cfg[$key] = $cfg;
			return $top_cfg;
		}
	}

	// Genera un div para el preview de una coleccion.
	// source_items es texto plano, separado por coma. Y el preview consiste en mostrar la cantidad de items.
	if ( ! function_exists('generate_page_collection_preview'))
	{
		function generate_page_collection_preview($source_items, $refresh_reference) 
		{
			// Obtenemos la estructura de configuracion.
			$CI =& get_instance();

			if (strlen($source_items) > 0)
				$data['collection']	= explode(',', $source_items);
			else
				$data['collection'] = array();

			$data['reference']	= $refresh_reference;
			return $CI->load->view('nawglobe/components/gtp_collection_page_preview', $data, TRUE);
		}
	}

	// Genera un div con los previews de imagenes de una determinada fuente. 
	// source_items es texto plano, separado por comas. Y el previw son imagenes en miniatura de los items.
	if ( ! function_exists('generate_media_collection_preview'))
	{
		function generate_media_collection_preview($source_items, $refresh_reference) 
		{
			// Obtenemos la estructura de configuracion.
			$CI =& get_instance();

			if (strlen($source_items) > 0 AND $source_items != 0)
				$data['collection']	= explode(',', $source_items);
			else
				$data['collection'] = array();

			$data['reference']	= $refresh_reference;
			return $CI->load->view('nawglobe/components/gtp_collection_preview', $data, TRUE);
		}
	}

	if ( ! function_exists('generate_media_gallery'))
	{
		// Valid mediatypes: image, video.
		function generate_media_gallery($pickable = FALSE, $media_type = 'all', $selection_sort = NULL) 
		{
			// Obtenemos la estructura de configuracion.
			$CI =& get_instance();
			$CI->lang->load('media_lang', $CI->session->userdata('user_language'));
			
			$data['media_items']			= $CI->media->get_media(FALSE, FALSE, 0, PAGINATION_MEDIA_LIST, $media_type, $selection_sort);
			$data['pickable']				= $pickable;
			$data['type_filter']			= $media_type;
			$data['media_groups']			= $CI->media->get_groups();
			return $CI->load->view('nawglobe/pages/media/components/media_gallery', $data, TRUE);
		}
	}

	if ( ! function_exists('generate_multi_configuration_form'))
	{
		function generate_multi_configuration_form($scope, $reference = 0) 
		{
			// Obtenemos la estructura de configuracion.
			$CI =& get_instance();
			$languages = $CI->config->item('available_languages');
			
			$data['languages'] 			= $languages;
			$data['config_scope'] 		= $scope;
			$data['config_reference'] 	= (int)$reference;
			return $CI->load->view('nawglobe/components/gtp_multi_configuration_form', $data, TRUE);
		}
	}

	if ( ! function_exists('generate_localized_configuration_form'))
	{
		function generate_localized_configuration_form($scope, $config_reference, $lang, $simple_design = FALSE) 
		{
			// Obtenemos la estructura de configuracion.
			$CI =& get_instance();
			$CI->lang->load('media_lang', $CI->session->userdata('user_language'));
			$languages = $CI->config->item('available_languages');
			
			$data['config_scope'] 		= $CI->configurations->get_structure_and_setted_configurations($scope, $config_reference, $lang);
			$data['scope'] 				= $scope;
			$data['config_reference'] 	= (int)$config_reference;
			$data['languages'] 			= $languages;
			$data['lang'] 				= $lang;

			if ($simple_design) 
				return $CI->load->view('nawglobe/components/gtp_configuration_form_basic', $data, TRUE);
			else
				return $CI->load->view('nawglobe/components/gtp_configuration_form', $data, TRUE);
		}
	}

	if ( ! function_exists('check_permissions'))
	{
		function check_permissions($permission_group, $permission_item, $id_user = FALSE, $interrupt = FALSE) 
		{
			$CI =& get_instance();
			if ($id_user == FALSE) $id_user = (int)$CI->session->userdata('id_user');
			if ($id_user == 0) return FALSE;

			$permission = $CI->permissions->get($id_user, $permission_group, $permission_item);

			if ($interrupt == TRUE AND $permission == FALSE)
				ajax_response('success', $CI->lang->line('general_no_permission'), '1', '1');
			else
				return $permission;
		}
	}

	if ( ! function_exists('get_user_name'))
	{
		function get_user_name($id_user = FALSE) 
		{
			$CI =& get_instance();

			$user = $CI->users->get_user($id_user);

			return (empty($user['user_name'])) ? 'N/D' : $user['user_name'];
		}
	}

	if ( ! function_exists('handle_filters'))
	{
		function handle_filters($filter) 
		{
			if (empty($filter) OR $filter == FALSE)
				return FALSE;
			else
			{	
				$filters = explode(';', $filter);
				$cond = array();

				foreach ($filters as $key => $filter) 
				{
					$filter_segments = explode(':', $filter);

					if (count($filter_segments) > 1) 
					{
						$filter_segment_value = explode(',', $filter_segments[1]);

						if (count($filter_segment_value)>1) {

							foreach ($filter_segment_value as $key => $filter_segment_value_item) 
							{
								$cond[$filter_segments[0]][] = strtolower($filter_segment_value_item) == 'null' ? NULL : $filter_segment_value_item;
							}
							
						}else{

							if ($filter_segments[1] == 'notnull')
								$cond[$filter_segments[0].'!='] = NULL;
							else
								$cond[$filter_segments[0]] = (strtolower($filter_segments[1]) == 'null') ? NULL : $filter_segments[1];
						}
					}
				}
				return $cond;
			}
		}
	}
	
/* End of file gestorp_helper.php */
/* Location: ./application/controllers/gestorp_helper.php */