<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Chequeados
 */

	// Retorna el tiempo hasta una determinada fecha.
	// NOTA: Hacerla multi lenguaje.
	if ( ! function_exists('get_time_ago'))
	{
		function get_time_ago($date, $full = FALSE) 
		{
			$tz  	= new DateTimeZone('America/Argentina/Buenos_Aires');
			$bday 	= DateTime::createFromFormat('Y-m-d H:i:s', $date, $tz);
			$today 	= new DateTime('now', $tz);
			$diff 	= $today->diff($bday);

			$tiempo 	= NULL;
			$years 	= $diff->y;
			$meses 	= $diff->m;
			$dias 	= $diff->d;
			$horas 	= $diff->h;
			$minutos= $diff->i;

			#return "$years $meses $dias m$horas -$minutos";

			if ($years > 0)
				$tiempo .= $years.' año';

			if ($years > 1)
				$tiempo .= 's';

			if ($meses == 1)
				$tiempo .= ' 1 mes';

			if ($meses > 1)
				$tiempo .= ' '.$meses.' meses';

			if ($dias > 0)
				$tiempo .= ' '.$dias.' dia';

			if ($dias > 1)
				$tiempo .= 's';

			if (($horas > 0 AND $years == 0 AND $meses == 0) OR ($horas > 0 AND $full == TRUE))
				$tiempo .= ' '.$horas.' hora';

			if (($horas > 1 AND $years == 0 AND $meses == 0) OR ($horas > 1 AND $full == TRUE))
				$tiempo .= 's';

			if (($minutos > 0 AND $years == 0 AND $meses == 0) OR ($minutos > 0 AND $full == TRUE))
				$tiempo .= ' '.$minutos.' minuto';

			if (($minutos > 1 AND $years == 0 AND $meses == 0) OR ($minutos > 1 AND $full == TRUE))
				$tiempo .= 's';

			return $tiempo;
		}
	}

	// Retorna la URL de la imagen del usuario
	if ( ! function_exists('get_avatar'))
	{
		function get_avatar($id_user = FALSE) 
		{
			$CI =& get_instance();

			$id_user = ($id_user == FALSE) ? $CI->session->userdata('id_user') : FALSE;

			if ($id_user == $CI->session->userdata('id_user'))
			{
				$user_picture = $CI->session->userdata('user_picture');
				return (empty($user_picture)) ? THEME_ASSETS_URL.'nawglobe/default_user.png' : UPLOADS_URL.'u/'.$user_picture;
			}
			else
			{
				$user = $CI->users->get_user($id_user);
				$user_picture = $user['user_picture'];
				return (empty($user_picture)) ? THEME_ASSETS_URL.'nawglobe/default_user.png' : UPLOADS_URL.'u/'.$user_picture;
			}

		}
	}

	if ( ! function_exists('nwg_assets'))
	{
		function nwg_assets($path = NULL) 
		{
			return '/assets/'.$path;
		}
	}

	if ( ! function_exists('nwg_assets_theme'))
	{
		function nwg_assets_theme($url = NULL) 
		{
		  	return THEME_ASSETS_URL.$url;
		}
	}

/**
 * Revisar uso de los siguientes
 */


	if ( ! function_exists('debugger'))
	{
		function debugger($array, $exit = FALSE) 
		{
		  	$taran = "<pre>".print_r($array, TRUE)."</pre>";
		  	
		  	if ($exit)
		  		exit($taran);

		  	echo $taran;
		}
	}

	if ( ! function_exists('static_url'))
	{
		function static_url($path = NULL, $internal = TRUE) 
		{
		  	return nwg_assets($path);
		}
	}

	if ( ! function_exists('theme_url'))
	{
		function theme_url($url = NULL) 
		{
		  	return static_url(GESTORP_THEME.'/'.$url, FALSE);
		}
	}

	if ( ! function_exists('uploads_url'))
	{
		function uploads_url($url = NULL) 
		{
		  	return UPLOADS_URL.$url;
		}
	}

	if ( ! function_exists('show_page'))
	{
		function show_page($page, $data = array(), $layout = 'general') 
		{
			$CI =& get_instance();

			// Identificamos si se esta mostrando la base o una subpagina
			$sections = explode('/', $page);
				$page = $sections[0];

			if (count($sections) > 1)
			{
				$default_view = (isset($sections[2])) ? $sections[2] : 'main';
				$section = 'subpages/'.$sections[1].'/'.$default_view;
			}
			else
				$section = 'main';

			// Vemos si hay un archivo de recursos para cargar
			$resouce_file = APPPATH.'views/nawglobe/'.DIRECTORY_SEPARATOR.'pages'.DIRECTORY_SEPARATOR.$page.DIRECTORY_SEPARATOR.'custom_page_resources.php';
			if (file_exists($resouce_file))
			{
				include($resouce_file);
				$data['RESOURCES'] = @$RESOURCES;
				$data['COMPONENTS'] = @$COMPONENTS;
			}
			else
			{
				$data['RESOURCES'] = FALSE;
				$data['COMPONENTS'] = FALSE;
			}

			// Cargamos los datos de la pagina y subpagina que se esta mostrando
			define('PAGE', strtolower($page));
			define('SUBPAGE', strtolower(@$sections[1]));

			// Cargamos la pagina a mostrar
			$data['CONTENT'] = $CI->load->view("nawglobe/pages/$page/$section", $data, TRUE);
			$CI->load->view("nawglobe/layouts/$layout/layout", array('data' => $data));
		}
	}

	// Esta funcion solo puede ser llamada desde vistas mostradas con show_page
	if ( ! function_exists('load_page_piece'))
	{
		function load_page_piece($component, $data = array(), $return = FALSE) 
		{
			$CI =& get_instance();

			$resouce_file = APPPATH.'views/nawglobe/'.DIRECTORY_SEPARATOR.'pages'.DIRECTORY_SEPARATOR.PAGE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.$component.'.php';
			$view_file = 'nawglobe/pages'.DIRECTORY_SEPARATOR.PAGE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.$component;
			if (file_exists($resouce_file))
				if (count($data) > 0)
					return $CI->load->view($view_file, $data, $return);
				else
					return $CI->load->view($view_file, FALSE, $return);
			else
				echo '{Error al cargar el componente}';

		}
	}

	if ( ! function_exists('text_preview'))
	{
		function text_preview($text, $max_lenght = 100, $final_string = "...") 
		{
			$return = strip_tags(trim($text));
	        if (strlen($return) >= $max_lenght) 
	            $return = substr($return, 0, $max_lenght - strlen($final_string)).$final_string;
	        return preg_replace("/&#?[a-z0-9]+;áéíóúñ/i","",$return);
		}
	}

	if ( ! function_exists('date_to_view'))
	{
		function date_to_view($date, $new_separator = "/", $intial_separator = "-", $human = FALSE) 
		{
			$partes = explode($intial_separator, substr($date, 0, 10));

			if ($human == TRUE)
			{
				$anio 	= $partes[0];
				$mes 	= $partes[1];
				$dia 	= $partes[2];
				return get_month_name($mes).' '.$dia.', '.$anio;
			}
			else
				return implode($new_separator, array_reverse($partes));
		}
	}

	if ( ! function_exists('check_date'))
	{
		function check_date($date){

			if (empty($date)) return FALSE;

			$separator 	= (strpos('-', $date) !== FALSE) ? '-' : '/';
			$date_parts = explode($separator, $date);
			
			if (count($date_parts) !== 3) return FALSE;

			// Vemos si la fecha esta en formato ddmmyyyy o al revez.
			if (strlen($date_parts[0]) == 4)
			{
				// Formato DB
			    $year = (int) $date_parts[0];
			    $month = (int) $date_parts[1];
				$day = (int) $date_parts[2];
			}
			else
			{
				// Formato Presentación
			    $day = (int) $date_parts[0];
			    $month = (int) $date_parts[1];
			    $year = (int) $date_parts[2];
			}
		    return checkdate($month, $day, $year);
		}
	}

	if ( ! function_exists('transform_date')) {
		function transform_date($date, $new_separator = '/', $intial_separator = '-') {
			return implode($new_separator, array_reverse(explode($intial_separator, substr($date, 0, 10))));
		}
	}

	if ( ! function_exists('time_to_view'))
	{
		function time_to_view($date) 
		{
			return substr($date, 11, 5);
		}
	}

	if ( ! function_exists('get_month_name'))
	{
		function get_month_name($month_number) 
		{
			$months = array('enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');
			return $months[(int)$month_number-1];
		}
	}

	if ( ! function_exists('ajax_response'))
	{
		function ajax_response($tipo, $descripcion, $code = null, $display = 1, $extra = null) 
		{
			echo json_encode(array('tipo' 	=> $tipo,
									 'mensaje' 	=> $descripcion,
									 'cod' 	=> $code,
									 'display' => $display,
									 'ext' 	=> $extra
									 )
								);
			exit();
		}
	}
	
	if ( ! function_exists('slug'))
	{
		function slug($text) 
		{
			if (is_array($text)) return 'error';
	        return url_title(strtolower(quitar_acentos($text)));
		}
	}

	if ( ! function_exists('quitar_acentos'))
	{
		function quitar_acentos($string) 
		{
			$a = array('á', 'é', 'í', 'ó', 'ú', 'ñ');
			$b = array('a', 'e', 'i', 'o', 'u', 'n');		
		  	return str_replace($a, $b, $string);
		}
	}

	if ( ! function_exists('static_square_pic'))
	{
		function static_square_pic($image_path, $class = NULL, $max_width = FALSE) 
		{
			$image_path = static_url($image_path);
			$max_width = ($max_width !== FALSE) ? 'max-width: '.$max_width.';' : NULL;
			return "
				<div class='xcpp $class' style='$max_width'>
				    <div class='xdsq'></div>
				    <div class='xpcc' style='background-image: url(\"$image_path\")'>
				    </div>
				</div>
			";
		}
	}

	if ( ! function_exists('static_rectangle_pic'))
	{
		function static_rectangle_pic($image_path, $class = NULL, $max_width = FALSE, $extras = FALSE) 
		{
			$image_path = static_url($image_path);
			$max_width = ($max_width !== FALSE) ? 'max-width: '.$max_width.';' : NULL;
			return "
				<div class='xcpp $class' style='$max_width' $extras>
				    <div class='xdrc'></div>
				    <div class='xpcc' style='background-image: url(\"$image_path\")'>
				    </div>
				</div>
			";
		}
	}

	if ( ! function_exists('generate_pagination'))
	{
		function generate_pagination($pagination_current, $pagination_total_items, $pagination_per_page) 
		{
			$data['pagination_current'] 	= $pagination_current;
			$data['pagination_total_items'] = $pagination_total_items;
			$data['pagination_per_page'] 	= $pagination_per_page;

			$CI =& get_instance();

			return $CI->load->view('nawglobe/components/pagination', $data, TRUE);
		}
	}

	if ( ! function_exists('generate_string'))
	{
		function generate_string($length) {
		    $char = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		    $char = str_shuffle($char);
		    for($i = 0, $rand = '', $l = strlen($char) - 1; $i < $length; $i ++) {
		        $rand .= $char{mt_rand(0, $l)};
		    }
		    return $rand;
		}
	}

	if ( ! function_exists('get_user_ip'))
	{
		function get_user_ip() 
		{

		    $ipaddress = '';
		    if (getenv('HTTP_CLIENT_IP'))
		        $ipaddress = getenv('HTTP_CLIENT_IP');
		    else if(getenv('HTTP_X_FORWARDED_FOR'))
		        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		    else if(getenv('HTTP_X_FORWARDED'))
		        $ipaddress = getenv('HTTP_X_FORWARDED');
		    else if(getenv('HTTP_FORWARDED_FOR'))
		        $ipaddress = getenv('HTTP_FORWARDED_FOR');
		    else if(getenv('HTTP_FORWARDED'))
		       $ipaddress = getenv('HTTP_FORWARDED');
		    else if(getenv('REMOTE_ADDR'))
		        $ipaddress = getenv('REMOTE_ADDR');
		    else
		        $ipaddress = 'UNKNOWN';

			return $ipaddress;
		}
	}

	if ( ! function_exists('generate_get_string'))
	{
		function generate_get_string($add_key = FALSE, $add_value =FALSE, $except = array()) {
		    $get_vars = $_GET;
		    $key_pairs = array();
		    foreach ($get_vars as $key => $value) 
		    	if (!in_array($key, $except) AND $key !== $add_key)
		    		array_push($key_pairs, $key.'='.$value)	;

		    if ($add_key !== FALSE)
		    	if (is_array($add_key))
		    	{
		    		foreach ($add_key as $key => $value) {
		    			array_push($key_pairs, $key.'='.$value);
		    		}
		    	}
		    	else
		    		array_push($key_pairs, $add_key.'='.$add_value);

		    if (count($key_pairs) > 0)
		    	return '?'.implode('&', $key_pairs);

		    return NULL;
		}
	}

	if ( ! function_exists('array_column'))
	{
	    function array_column($source, $column)
	    { 
		     $namearray = array();
		     foreach ($source as $name) {
		        $namearray[] = $name[$column];
		     }
		     return $namearray;
	    }
	}

	if ( ! function_exists('spanish_month'))
	{
	    function spanish_month($number)
	    { 
	        $spanish_months = array(
	            'January' => 'enero',
	            'February' => 'febrero',
	            'March' => 'marzo',
	            'April' => 'abril',
	            'May' => 'mayo',
	            'June' => 'junio',
	            'July' => 'julio',
	            'August' => 'agosto',
	            'September' => 'septiembre',
	            'October' => 'octubre',
	            'November' => 'noviembre',
	            'December' => 'diciembre'
	        );

	        return @$spanish_months[$number];;
	    }
	}

	if ( ! function_exists('spanish_day'))
	{
	    function spanish_day($name, $return_number = FALSE)
	    { 
	        $spanish_days = array(
	            'Mon' => 'Lun',
	            'Tue' => 'Mar',
	            'Wed' => 'Mie',
	            'Thu' => 'Jue',
	            'Fri' => 'Vie',
	            'Sat' => 'Sab',
	            'Sun' => 'Dom',
	        );

	        if ($return_number == TRUE)
	        {
	        	$weekday_number = 0;
	        	foreach ($spanish_days as $key => $day) 
	        	{
	        		$weekday_number++;
	        		if ($name == $key) return $weekday_number;
	        	}
	        }

	        return @$spanish_days[$name];;
	    }
	}

	if ( ! function_exists('generate_session_token'))
	{
		function generate_session_token($id_user, $email) 
		{
			#echo "$id_user - $email - ".md5($id_user.':367efa31cbbf5483974b0a809a294679:'.$email).' <- FIN HELPER. <br>';
			return md5($id_user.':367efa31cbbf5483974b0a809a294679:'.$email);
		}
	}

	function minify_html($text) // 
	{
	    ini_set("pcre.recursion_limit", "16777");  // 8MB stack. *nix
	    $re = '%# Collapse whitespace everywhere but in blacklisted elements.
	        (?>             # Match all whitespaces other than single space.
	          [^\S ]\s*     # Either one [\t\r\n\f\v] and zero or more ws,
	        | \s{2,}        # or two or more consecutive-any-whitespace.
	        ) # Note: The remaining regex consumes no text at all...
	        (?=             # Ensure we are not in a blacklist tag.
	          [^<]*+        # Either zero or more non-"<" {normal*}
	          (?:           # Begin {(special normal*)*} construct
	            <           # or a < starting a non-blacklist tag.
	            (?!/?(?:textarea|pre|script)\b)
	            [^<]*+      # more non-"<" {normal*}
	          )*+           # Finish "unrolling-the-loop"
	          (?:           # Begin alternation group.
	            <           # Either a blacklist start tag.
	            (?>textarea|pre|script)\b
	          | \z          # or end of file.
	          )             # End alternation group.
	        )  # If we made it here, we are not in a blacklist tag.
	        %Six';
	    $minified_html = preg_replace($re, " ", $text);
	    if ($minified_html === null) return $text;
	    
	    // Eliminamos los comentarios dejando condicionales.
	    $minified_html = preg_replace('#<!--[^\\[<>].*?(?<!!)-->#s', '', $minified_html);

	    // Le agregamos nuestra firma.
	    // Desarrollo del gestor de contenidos. Proyecto gestionado por Inglobe, y programaado por Pablo Chirino.
	    if (defined('PUBLIC_SIGNATURE')) 
	    	$minified_html .= PHP_EOL.'<!-- '.PHP_EOL.PUBLIC_SIGNATURE.PHP_EOL.' -->';
	    
	    return $minified_html;
	}

	/**
	 * 
	 * @param Array $list
	 * @param int $p
	 * @return multitype:multitype:
	 * @link http://www.php.net/manual/en/function.array-chunk.php#75022
	 */
	function partition(Array $list, $p) {
	    $listlen = count($list);
	    $partlen = floor($listlen / $p);
	    $partrem = $listlen % $p;
	    $partition = array();
	    $mark = 0;
	    for($px = 0; $px < $p; $px ++) {
	        $incr = ($px < $partrem) ? $partlen + 1 : $partlen;
	        $partition[$px] = array_slice($list, $mark, $incr);
	        $mark += $incr;
	    }
	    return $partition;
	}

/* End of file gestorp_helper.php */
/* Location: ./application/controllers/gestorp_helper.php */