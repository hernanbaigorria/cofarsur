<body class="page-md page-header-fixed page-sidebar-closed-hide-logo page-sidebar-fixed">
<!-- BEGIN HEADER -->
<div class="page-header md-shadow-z-1-i navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
	<!-- VERSION -->
	<div style="text-align:center;position: absolute;color: white;width: 100%;">
		<?php if ($page['published_version'] == $version): ?>
			<p class="margin-none"><?php echo $this->lang->line('pages_editing_live') ?></p>
		<?php else: ?>
			<a href="javascript:;" class="text-success btn-publish-version" data-page="<?php echo $page['id_page']; ?>" data-version="<?php echo $version ?>" data-confirmation="<?php echo $this->lang->line('pages_publish_version_sure') ?>"><?php echo $this->lang->line('pages_publish_version_editor'); ?></a>
		<?php endif ?>
		<!-- VERSION -->
	</div>
		<?php if (@$COMPONENTS['bar_header'] == TRUE): ?>
		<?php $this->load->view('nawglobe/layouts/editor/components/bar_header') ?>
		<?php endif ?>
	</div>
	<!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content padding-top-none fixed-height">
			<!-- BEGIN PAGE CONTENT-->
				<?php echo @$CONTENT; ?>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->

