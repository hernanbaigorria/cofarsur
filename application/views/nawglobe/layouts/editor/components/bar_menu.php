<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
<div class="page-sidebar md-shadow-z-2-i  navbar-collapse collapse">
	<!-- BEGIN SIDEBAR MENU -->
	<ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
		<li class="start open">
			<a href="javascript:;">
			<i class="icon-layers"></i>
			<span class="title"><?php echo $this->lang->line('pages_add_block') ?> </span>
			<span class="arrow "></span>
			</a>
			<ul class="sub-menu text-small" style="display: block;">
				<?php foreach ($avalilable_components as $key => $a_component): ?>
				<li>
					<a href="javascript:;" class="btn-add-block" data-component="<?php echo $a_component ?>" title="<?php echo strtolower(implode(' ', explode('_', $a_component))) ?>">
						<i class="icon-plus"></i>
						<?php echo text_preview(strtolower(implode(' ', explode('_', $a_component))), 25, '...') ?>
					</a>
				</li>
				<?php endforeach ?>
			</ul>
		</li>
	</ul>
	<!-- END SIDEBAR MENU -->
</div>