		<!-- BEGIN LOGO -->
		<div class="page-logo" >
			<a target="_blank" href="<?php echo base_url() ?>" class="text-center logo-default" style="background-image: url('<?php echo nwg_assets_theme('nawglobe/default_logo.png') ?>')">
			</a>
			<div class="menu-toggler sidebar-toggler hide">
				<!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
			</div>
		</div>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN PAGE TOP -->
		<div class="page-top">
			<!-- BEGIN TOP NAVIGATION MENU -->
			<div class="top-menu">
				<ul class="nav navbar-nav pull-right">
					<!-- BEGIN USER LOGIN DROPDOWN -->
					<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
					<li class="dropdown dropdown-user">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<img alt="" class="img-circle" src="<?php echo get_avatar() ?>"/>
						<span class="username username-hide-on-mobile">
						<?php echo $this->session->userdata('user_name'); ?> </span>
						<i class="fa fa-angle-down"></i>
						</a>
						<ul class="dropdown-menu dropdown-menu-default">
							<li>
								<a href="<?php echo base_url().GESTORP_MANAGER ?>/account">
								<i class="icon-wrench"></i> <?php echo $this->lang->line('general_account_configuration'); ?> </a>
							</li>
							<li class="divider">
							</li>
							<li>
								<a href="<?php echo base_url() ?>login/salir_manager/" class="btn-end-session">
									<i class="icon-exit"></i> <?php echo $this->lang->line('general_account_logout'); ?> 
								</a>
							</li>
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
			</div>
			<!-- END TOP NAVIGATION MENU -->
		</div>
		<!-- END PAGE TOP -->