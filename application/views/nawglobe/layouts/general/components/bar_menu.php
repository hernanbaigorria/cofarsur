<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
<div class="page-sidebar navbar-collapse collapse">
	<!-- BEGIN SIDEBAR MENU -->
	<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
	<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
	<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
	<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
	<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
	<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
	<ul class="page-sidebar-menu page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
		<?php $current_section = strtolower($this->uri->segment(2)); ?>
		<li class="start <?php if ($current_section == 'dashboard') echo 'active' ?>">
			<a href="<?php echo base_url().GESTORP_MANAGER ?>/dashboard">
			<i class="icon-home"></i>
			<span class="title"><?php echo $this->lang->line('general_dashboard'); ?></span>
			</a>
		</li>

		<?php if (check_permissions('gestorp', 'manage_pages')): ?>
			<li class="<?php if ($current_section == 'pages') echo 'active' ?>">
				<a href="<?php echo base_url().GESTORP_MANAGER ?>/pages">
					<i class="icon-docs"></i>
					<span class="title"><?php echo $this->lang->line('general_pages'); ?></span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">
					<li>
						<a href="<?php echo base_url().GESTORP_MANAGER ?>/pages/simples">
							Páginas Simples
						</a>
					</li>
					<li>
						<a href="<?php echo base_url().GESTORP_MANAGER ?>/pages/grouped">
							Páginas Agrupadas
						</a>
					</li>
					<li>
						<a href="<?php echo base_url().GESTORP_MANAGER ?>/pages/external">
							Links
						</a>
					</li>
				</ul>
			</li>
		<?php endif ?>

		<?php if (check_permissions('gestorp', 'manage_menus')): ?>
			<li class="hidden <?php if ($current_section == 'menus') echo 'active' ?>">
				<a href="<?php echo base_url().GESTORP_MANAGER ?>/menus">
				<i class="icon-list"></i>
				<span class="title"><?php echo $this->lang->line('general_menus'); ?></span>
				</a>
			</li>
		<?php endif ?>

		<?php if (check_permissions('gestorp', 'manage_modules')): ?>
			<?php $gtp_available_modules = get_modules(); ?>
			<?php if (count($gtp_available_modules) > 0): ?>
			<li class="<?php if ($current_section == 'modules') echo 'active' ?>">
				<a href="javascript:;">
					<i class="icon-puzzle"></i>
					<span class="title"><?php echo $this->lang->line('general_modules'); ?></span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">
					<?php foreach ($gtp_available_modules as $gtp_key => $gtp_module): ?>
					<li>
						<a href="<?php echo base_url().GESTORP_MANAGER ?>/modules/<?php echo $gtp_module ?>">
							<?php echo ucfirst(implode(' ', explode('_', $gtp_module))); ?>
						</a>
					</li>
					<?php endforeach ?>
				</ul>
			</li>
			<?php endif ?>
		<?php endif ?>

		<?php if (check_permissions('gestorp', 'manage_media')): ?>
			<li class="<?php if ($current_section == 'media') echo 'active' ?>">
				<a href="<?php echo base_url().GESTORP_MANAGER ?>/media">
				<i class="icon-social-dropbox"></i>
				<span class="title"><?php echo $this->lang->line('general_media'); ?></span>
				</a>
			</li>
		<?php endif ?>

		<?php if (check_permissions('gestorp', 'manage_configurations')): ?>
			<li class="<?php if ($current_section == 'configuration') echo 'active' ?>">
				<a href="<?php echo base_url().GESTORP_MANAGER ?>/configuration">
				<i class="icon-wrench"></i>
				<span class="title"><?php echo $this->lang->line('general_configuration'); ?></span>
				</a>
			</li>
		<?php endif ?>

		<?php if (check_permissions('gestorp', 'manage_users')): ?>
			<li class="last <?php if ($current_section == 'users') echo 'active' ?>">
				<a href="<?php echo base_url().GESTORP_MANAGER ?>/users">
				<i class="icon-users"></i>
				<span class="title"><?php echo $this->lang->line('general_users'); ?></span>
				</a>
			</li>
		<?php endif ?>

		<?php if ($this->session->userdata('user_dev')): ?>
			<li class="last <?php if ($current_section == 'users') echo 'active' ?>">
				<a href="<?php echo base_url().GESTORP_MANAGER ?>/system">
				<i class="icon-fire"></i>
				<span class="title"><?php echo $this->lang->line('general_system'); ?></span>
				</a>
			</li>
		<?php endif ?>

	</ul>
	<!-- END SIDEBAR MENU -->
</div>