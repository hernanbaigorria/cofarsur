<!-- BEGIN FOOTER -->
<?php if (@$COMPONENTS['footer'] == TRUE): ?>
	<div class="container">
		<div class="page-footer">
			<div class="page-footer-inner">
				2015-<?php echo date('Y') ?> &copy; <?php echo NAWGLOBE_NAME ?> v<?php echo NAWGLOBE_VERSION ?>. <a href="#" target="_blank"></a> <?php if (strlen(GESTORP_SIGNATURE) > 0) echo ' - '.GESTORP_SIGNATURE ?>
			</div>
			<div class="scroll-to-top">
				<i class="icon-arrow-up"></i>
			</div>
		</div>
	</div>
<?php endif ?>
<!-- END FOOTER -->

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->
	<!--[if lt IE 9]>
	<script src="../../assets/plugins/respond.min.js"></script>
	<script src="../../assets/plugins/excanvas.min.js"></script> 
	<![endif]-->
	
	<script src="<?php echo nwg_assets('plugins/jquery-migrate.min.js') ?>" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="<?php echo nwg_assets('plugins/jquery-ui/jquery-ui.min.js') ?>" type="text/javascript"></script>
	<script src="<?php echo nwg_assets('plugins/bootstrap/js/bootstrap.min.js') ?>" type="text/javascript"></script>
	<script src="<?php echo nwg_assets('plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') ?>" type="text/javascript"></script>
	<script src="<?php echo nwg_assets('plugins/jquery-slimscroll/jquery.slimscroll.min.js') ?>" type="text/javascript"></script>
	<script src="<?php echo nwg_assets('plugins/jquery.blockui.min.js') ?>" type="text/javascript"></script>
	<script src="<?php echo nwg_assets('plugins/jquery.cokie.min.js') ?>" type="text/javascript"></script>
	<script src="<?php echo nwg_assets('plugins/uniform/jquery.uniform.min.js') ?>" type="text/javascript"></script>
	<script src="<?php echo nwg_assets('plugins/bootstrap-switch/js/bootstrap-switch.min.js') ?>" type="text/javascript"></script>
	<script src="<?php echo nwg_assets('plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') ?>" type="text/javascript"></script>
	<script src="<?php echo nwg_assets('plugins/bootstrap-growl/jquery.bootstrap-growl.min.js') ?>" type="text/javascript"></script>
	<script src="<?php echo nwg_assets('plugins/bootbox/bootbox.min.js') ?>" type="text/javascript"></script>
	<script src="<?php echo nwg_assets('plugins/jquery-slimscroll/jquery.slimscroll.min.js') ?>" type="text/javascript"></script>
	<script src="<?php echo nwg_assets('plugins/jquery-inputmask/jquery.inputmask.bundle.min.js') ?>" type="text/javascript"></script>
	<script src="<?php echo nwg_assets('plugins/jquery-minicolors/jquery.minicolors.min.js') ?>" type="text/javascript"></script>
	
	<script src="<?php echo nwg_assets('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') ?>" type="text/javascript"></script>
	<script src="<?php echo nwg_assets('plugins/bootstrap-summernote/summernote.min.js') ?>" type="text/javascript"></script>
	
	<?php # REQUERIDOS en todos lados, generalmente para modales generados. ?>
	<script src="<?php echo nwg_assets('plugins/jstree/dist/jstree.min.js') ?>" type="text/javascript"></script>
	
	<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
	<script src="<?php echo nwg_assets('plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js') ?>"></script>
	<!-- The Templates plugin is included to render the upload/download listings -->
	<script src="<?php echo nwg_assets('plugins/jquery-file-upload/js/vendor/tmpl.min.js') ?>"></script>
	<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
	<script src="<?php echo nwg_assets('plugins/jquery-file-upload/js/vendor/load-image.min.js') ?>"></script>
	<!-- The Canvas to Blob plugin is included for image resizing functionality -->
	<script src="<?php echo nwg_assets('plugins/jquery-file-upload/js/vendor/canvas-to-blob.min.js') ?>"></script>
	<!-- blueimp Gallery script -->
	<script src="<?php echo nwg_assets('plugins/jquery-file-upload/blueimp-gallery/jquery.blueimp-gallery.min.js') ?>"></script>
	<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
	<script src="<?php echo nwg_assets('plugins/jquery-file-upload/js/jquery.iframe-transport.js') ?>"></script>
	<!-- The basic File Upload plugin -->
	<script src="<?php echo nwg_assets('plugins/jquery-file-upload/js/jquery.fileupload.js') ?>"></script>
	<!-- The File Upload processing plugin -->
	<script src="<?php echo nwg_assets('plugins/jquery-file-upload/js/jquery.fileupload-process.js') ?>"></script>
	<!-- The File Upload image preview & resize plugin -->
	<script src="<?php echo nwg_assets('plugins/jquery-file-upload/js/jquery.fileupload-image.js') ?>"></script>
	<!-- The File Upload audio preview plugin -->
	<script src="<?php echo nwg_assets('plugins/jquery-file-upload/js/jquery.fileupload-audio.js') ?>"></script>
	<!-- The File Upload video preview plugin -->
	<script src="<?php echo nwg_assets('plugins/jquery-file-upload/js/jquery.fileupload-video.js') ?>"></script>
	<!-- The File Upload validation plugin -->
	<script src="<?php echo nwg_assets('plugins/jquery-file-upload/js/jquery.fileupload-validate.js') ?>"></script>
	<!-- The File Upload user interface plugin -->
	<script src="<?php echo nwg_assets('plugins/jquery-file-upload/js/jquery.fileupload-ui.js') ?>"></script>
	<!-- The main application script -->
	<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
	<!--[if (gte IE 8)&(lt IE 10)]>
	<script src="<?php echo nwg_assets('plugins/jquery-file-upload/js/cors/jquery.xdr-transport.js') ?>"></script>
	<![endif]-->
    <script src="<?php echo nwg_assets('plugins/codemirror/lib/codemirror.js') ?>" type="text/javascript"></script>
    <script src="<?php echo nwg_assets('plugins/codemirror/mode/javascript/javascript.js') ?>" type="text/javascript"></script>
    <script src="<?php echo nwg_assets('plugins/codemirror/mode/htmlmixed/htmlmixed.js') ?>" type="text/javascript"></script>
    <script src="<?php echo nwg_assets('plugins/codemirror/mode/css/css.js') ?>" type="text/javascript"></script>
	<?php # Fin de requieridos en todos lados.. ?>

	<script src="<?php echo nwg_assets('pages/js/ajax_engine.js') ?>" type="text/javascript"></script>
	<script src="<?php echo nwg_assets('pages/js/common.js') ?>" type="text/javascript"></script>
	<script src="<?php echo nwg_assets('pages/js/gtp_general.js') ?>" type="text/javascript"></script>
	<script src="<?php echo nwg_assets('pages/js/gtp_media.js') ?>" type="text/javascript"></script>
	<!-- END CORE PLUGINS -->

	<?php if ($RESOURCES !== FALSE): ?>
	<!-- BEGIN: Page level plugins -->
		<?php foreach ($RESOURCES['footer_js'] as $file => $resource): ?>
			<?php if (is_array($resource)): ?> 
				<?php if (in_array(SUBPAGE, $resource['subpages'])): ?>
					<?php if (strpos($resource['link'], 'http://') === FALSE): ?>
						<script src="<?php echo nwg_assets($resource['link']) ?>" type="text/javascript"></script>
					<?php else: ?>
						<script src="<?php echo $resource['link'] ?>" type="text/javascript"></script>
					<?php endif ?>
				<?php endif ?>
			<?php else: ?>
				<?php if (strpos($resource, 'http://') === FALSE): ?>
					<script src="<?php echo nwg_assets($resource) ?>" type="text/javascript"></script>
				<?php else: ?>
					<script src="<?php echo $resource ?>" type="text/javascript"></script>
				<?php endif ?>
			<?php endif ?>
		<?php endforeach ?>
	<!-- END: Page level plugins -->
	<?php endif ?>

	<script src="<?php echo nwg_assets('layout/js/metronic.js') ?>" type="text/javascript"></script>
	<script src="<?php echo nwg_assets('layout/js/layout_general.js') ?>" type="text/javascript"></script>
	<script>
		jQuery(document).ready(function() {    
			Metronic.init(); // init metronic core components
			Layout.init(); // init current layout
		});
	</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
