<div class="tabbable-line">
	<ul class="nav nav-tabs">
		<?php if (count($languages) > 1): ?>
		<?php foreach ($languages as $key => $lang): ?>
			<li class="<?php if ($key == 0) echo "active"; ?>">
				<a href="#tab_cfg_<?php echo $lang ?>" data-toggle="tab" aria-expanded="true">
					<?php echo ucfirst($lang) ?> 
				</a>
			</li>
		<?php endforeach ?>
		<?php endif ?>
	</ul>
	<div class="tab-content <?php if (count($languages) <= 1) echo 'border-none' ?>">
		<?php foreach ($languages as $key => $lang): ?>
		<div class="tab-pane <?php if ($key == 0) echo "active in"; ?>" id="tab_cfg_<?php echo $lang ?>">
			<?php echo generate_localized_configuration_form($config_scope, $config_reference, $lang); ?>
		</div>
		<?php endforeach ?>
	</div>
</div>