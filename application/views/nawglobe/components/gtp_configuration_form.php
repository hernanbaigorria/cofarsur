<form role="form" class="frm_save_configurations">
	<div class="form-body">
		<input type="hidden" name="config_scope" 	value="<?php echo $scope; ?>">
		<input type="hidden" name="config_reference"value="<?php echo $config_reference; ?>">
		<input type="hidden" name="config_lang"		value="<?php echo $lang; ?>">
		
		<?php if (is_array($config_scope)): ?>
			<?php foreach ($config_scope as $key => $config_item): ?>

				<?php 
					if ($config_item['permissions'] !== FALSE) 
					{
						if (is_array($config_item['permissions']))
						{
							foreach ($config_item['permissions'] as $pkey => $permission) 
		                        if(!check_permissions($scope, $permission, FALSE)) 
		                        	continue;
						}
						else
						{
							if(!check_permissions($scope, $config_item['permissions'], FALSE)) 
								continue;
						}
					}
				 ?>

				<?php if ($config_item['type'] == 'text'): ?>
					<?php if (isset($config_item['setted'])): ?>
						<?php $setted = $config_item['setted']['config_value']; ?>
					<?php else: ?>
						<?php $setted = NULL; ?>
					<?php endif ?>
					<div class="form-group text-sm">
						<label for="txt_<?php echo $key ?>">
							<?php echo $this->lang->line('configurations_'.$key); ?>
							<?php if ($config_item['multilang'] == true): ?>
								<i class="fa fa-globe" title="<?php echo $this->lang->line('general_allow_translations'); ?>"></i>
							<?php endif ?>
						</label>
						<input <?php if (@$config_item['required'] == TRUE) echo 'required' ?> name="<?php echo $key ?>" type="text" class="form-control gtp-form-control input-sm <?php if (isset($config_item['mask'])) echo $config_item['mask'] ?>" id="txt_<?php echo $key ?>" placeholder="<?php echo $this->lang->line('general_write_here'); ?>" value="<?php echo $setted ?>">
					</div>
				<?php endif ?>


				<?php if ($config_item['type'] == 'date'): ?>
					<?php if (isset($config_item['setted'])): ?>
						<?php $setted = $config_item['setted']['config_value']; ?>
					<?php else: ?>
						<?php $setted = NULL; ?>
					<?php endif ?>
					<div class="form-group text-sm">
						<label for="txt_<?php echo $key ?>">
							<?php echo $this->lang->line('configurations_'.$key); ?>
							<?php if ($config_item['multilang'] == true): ?>
								<i class="fa fa-globe" title="<?php echo $this->lang->line('general_allow_translations'); ?>"></i>
							<?php endif ?>
						</label>
						<input <?php if (@$config_item['required'] == TRUE) echo 'required' ?> name="<?php echo $key ?>" type="date" class="form-control gtp-form-control input-sm <?php if (isset($config_item['mask'])) echo $config_item['mask'] ?>" id="txt_<?php echo $key ?>" placeholder="<?php echo $this->lang->line('general_write_here'); ?>" value="<?php echo $setted ?>">
					</div>
				<?php endif ?>

				<?php if ($config_item['type'] == 'textarea'): ?>
					<?php if (isset($config_item['setted'])): ?>
						<?php $setted = $config_item['setted']['config_value']; ?>
					<?php else: ?>
						<?php $setted = NULL; ?>
					<?php endif ?>
					<div class="form-group text-sm">
						<label for="txta_<?php echo $key ?>">
							<?php echo $this->lang->line('configurations_'.$key); ?>
							<?php if ($config_item['multilang'] == true): ?>
								<i class="fa fa-globe"></i>
							<?php endif ?>
						</label>
						<textarea <?php if (@$config_item['required'] == TRUE) echo 'required' ?> name="<?php echo $key ?>" class="form-control gtp-form-control <?php if (isset($config_item['mask'])) echo $config_item['mask'] ?>" id="txta_<?php echo $key ?>" rows="3" placeholder="<?php echo $this->lang->line('general_write_here'); ?>"><?php echo $setted; ?></textarea>
					</div>
				<?php endif ?>

				<?php if ($config_item['type'] == 'image'): ?>
					<?php if (isset($config_item['setted'])): ?>
						<?php $setted = $this->collections->get_collection_items($config_item['setted']['config_value']); ?>
					<?php else: ?>
						<?php $setted = NULL; ?>
					<?php endif ?>

					<div class="form-group text-sm">
						<label for="form_control_1"><?php echo $this->lang->line('configurations_'.$key); ?>
							<?php if ($config_item['multilang'] == true): ?>
								<i class="fa fa-globe" title="<?php echo $this->lang->line('general_allow_translations'); ?>"></i>
							<?php endif ?>
						</label>
						<div class="input-group">
							<span class="input-group-btn gbtn-left">
								<a class="gbtn gbtn-default gbtn-sm green btn-pick-media" data-max="<?php echo (int)@$config_item['max']; ?>" data-cfg="img_<?php echo $key.'_'.$lang ?>" data-filter="image">
									<?php echo $this->lang->line('general_select_images') ?>
								</a>
							</span>						
							<div class="input-group-control">
								<input id="img_<?php echo $key.'_'.$lang ?>" name="media_items|<?php echo $key ?>" type="hidden" class="form-control gtp-form-control" placeholder="<?php echo $this->lang->line('general_nothing_selected'); ?>" value="<?php echo $setted ?>" >
								<?php echo generate_media_collection_preview($setted, "#img_".$key.'_'.$lang) ?>
							</div>
						</div>
					</div>
				<?php endif ?>

				<?php if ($config_item['type'] == 'video'): ?>
					<?php if (isset($config_item['setted'])): ?>
						<?php $setted = $this->collections->get_collection_items($config_item['setted']['config_value']); ?>
					<?php else: ?>
						<?php $setted = NULL; ?>
					<?php endif ?>

					<div class="form-group text-sm">
						<label for="form_control_1"><?php echo $this->lang->line('configurations_'.$key); ?>
							<?php if ($config_item['multilang'] == true): ?>
								<i class="fa fa-globe" title="<?php echo $this->lang->line('general_allow_translations'); ?>"></i>
							<?php endif ?>
						</label>
						<div class="input-group">
							<span class="input-group-btn gbtn-left">
								<a class="gbtn gbtn-default gbtn-sm green btn-pick-media" data-max="<?php echo (int)@$config_item['max']; ?>" data-cfg="img_<?php echo $key.'_'.$lang ?>" data-filter="video">
									<?php echo $this->lang->line('general_select_videos') ?>
								</a>
							</span>						
							<div class="input-group-control">
								<input id="img_<?php echo $key.'_'.$lang ?>" name="media_items|<?php echo $key ?>" type="hidden" class="form-control gtp-form-control" placeholder="<?php echo $this->lang->line('general_nothing_selected'); ?>" value="<?php echo $setted ?>" >
								<?php echo generate_media_collection_preview($setted, "#img_".$key.'_'.$lang) ?>
							</div>
						</div>
					</div>
				<?php endif ?>

				<?php if ($config_item['type'] == 'pdf'): ?>
					<?php if (isset($config_item['setted'])): ?>
						<?php $setted = $this->collections->get_collection_items($config_item['setted']['config_value']); ?>
					<?php else: ?>
						<?php $setted = NULL; ?>
					<?php endif ?>

					<div class="form-group text-sm">
						<label for="form_control_1"><?php echo $this->lang->line('configurations_'.$key); ?>
							<?php if ($config_item['multilang'] == true): ?>
								<i class="fa fa-globe" title="<?php echo $this->lang->line('general_allow_translations'); ?>"></i>
							<?php endif ?>
						</label>
						<div class="input-group">
							<span class="input-group-btn gbtn-left">
								<a class="gbtn gbtn-default gbtn-sm green btn-pick-media" data-max="<?php echo (int)@$config_item['max']; ?>" data-cfg="img_<?php echo $key.'_'.$lang ?>" data-filter="pdf">
									<?php echo $this->lang->line('general_select_documents') ?>
								</a>
							</span>						
							<div class="input-group-control">
								<input id="img_<?php echo $key.'_'.$lang ?>" name="media_items|<?php echo $key ?>" type="hidden" class="form-control gtp-form-control" placeholder="<?php echo $this->lang->line('general_nothing_selected'); ?>" value="<?php echo $setted ?>" >
								<?php echo generate_media_collection_preview($setted, "#img_".$key.'_'.$lang) ?>
							</div>
						</div>
					</div>
				<?php endif ?>

				<?php if ($config_item['type'] == 'page'): ?>
					<?php if (isset($config_item['setted'])): ?>
						<?php $setted = $this->collections->get_collection_items($config_item['setted']['config_value']); ?>
					<?php else: ?>
						<?php $setted = NULL; ?>
					<?php endif ?>

					<div class="form-group text-sm">
								<label for="form_control_1"><?php echo $this->lang->line('configurations_'.$key); ?>
									<?php if ($config_item['multilang'] == true): ?>
										<i class="fa fa-globe" title="<?php echo $this->lang->line('general_allow_translations'); ?>"></i>
									<?php endif ?>
								</label>
						<div class="input-group">
							<span class="input-group-btn gbtn-left">
								<a class="gbtn gbtn-default gbtn-sm green btn-pick-page" data-max="<?php echo (int)@$config_item['max']; ?>" data-cfg="pgs_<?php echo $key.'_'.$lang ?>">
									<?php echo $this->lang->line('general_select_pages') ?>
								</a>
							</span>						
							<div class="input-group-control">
								<input id="pgs_<?php echo $key.'_'.$lang ?>" name="page_items|<?php echo $key ?>" type="hidden" class="form-control gtp-form-control" placeholder="<?php echo $this->lang->line('general_nothing_selected'); ?>" value="<?php echo $setted ?>" data-lang="<?php echo $lang ?>">
								<?php echo generate_page_collection_preview($setted, "#pgs_".$key.'_'.$lang) ?>
							</div>
						</div>
					</div>
				<?php endif ?>

				<?php if ($config_item['type'] == 'checkbox'): ?>
					<?php if (isset($config_item['setted'])): ?>
						<?php $setted = $config_item['setted']['config_value']; ?>
					<?php else: ?>
						<?php $setted = NULL; ?>
					<?php endif ?>

                    <div class="innerTB">
	                    <div class="md-checkbox">
	                    	<input type="hidden" name="<?php echo $key ?>" value="0">
	                        <input <?php if (@$config_item['required'] == TRUE) echo 'required' ?> name="<?php echo $key ?>" value="1" type="checkbox" id="txt_<?php echo $key ?>" class="md-check <?php if (isset($config_item['mask'])) echo $config_item['mask'] ?>" <?php if (!empty($setted)) echo 'checked' ?>>
	                        <label for="txt_<?php echo $key ?>">
	                            <span></span>
	                            <span class="check"></span>
	                            <span class="box"></span> 
	                            <?php echo $this->lang->line('configurations_'.$key); ?>
								<?php if ($config_item['multilang'] == true): ?>
									<i class="fa fa-globe" title="<?php echo $this->lang->line('general_allow_translations'); ?>"></i>
								<?php endif ?>
	                        </label>
	                    </div>
                    </div>
				<?php endif ?>

			<?php endforeach ?>
		<?php endif ?>
	</div>
	<div class="form-actions noborder">
		<div class="separator only-on-modal hidden"></div>
		<button type="submit" class="gbtn gbtn-success"><?php echo $this->lang->line('general_save'); ?> <?php echo (count($languages) > 1) ? $lang : NULL; ?></button>
		<button type="button" data-dismiss="modal" class="gbtn gbtn-default only-on-modal hidden"><?php echo $this->lang->line('general_cancel') ?></button>
	</div>
</form>
