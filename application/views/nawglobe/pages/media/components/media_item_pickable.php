<?php $__name = (!empty($media_name)) ? $media_name : $media_file_name; ?>
<div class="col-lg-2 col-md-4 col-sm-3 col-xs-3 media-item pickable <?php if ($selected == TRUE) echo 'picked' ?>" data-media="<?php echo $id_media ?>" title="<?php echo $__name ?>">
	<a href="javascript:;" class="gbtn gbtn-icon-only btn-success pull-right btn-media-icn-picked">
		<i class="fa fa-check"></i>
	</a>

    	<?php if (strpos($media_type, 'video') !== FALSE): ?>
            <span class="media-thumbnail text-center" style="display: table;">
                <i class="fa fa-file-movie-o" style="font-size: 30px; display: table-cell; vertical-align: middle;"></i>
            </span>
		<?php else: ?>
	        <?php if (strpos($media_file_name, '.svg') !== FALSE): ?>
	            <span class="media-thumbnail" style="background-image: url('<?php echo UPLOADS_URL.$media_file_name ?>')"></span>
	        <?php else: ?>
    			<span class="media-thumbnail" style="background-image: url('<?php echo THUMBNAILS_URL.$media_file_name ?>')"></span>
	        <?php endif ?>
		<?php endif ?>

    <div class="name uppercase text-center"><small><?php echo text_preview($__name, 43, '...') ?></small></div>
</div>