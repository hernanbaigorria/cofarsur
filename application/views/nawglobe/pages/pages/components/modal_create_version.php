<!-- /.modal -->
<div class="modal fade bs-modal-md" id="modal-create-version" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<form role="form" id="frm-create-version">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title"><?php echo $this->lang->line('pages_page_new_version'); ?></h4>
				</div>
				<div class="modal-body">
					<input type="hidden" name="id_page" value="<?php echo $id_page ?>">
					<div class="form-group">
						<label class="control-label"><?php echo $this->lang->line('pages_create_from'); ?></label>
						<select name="from_version" class="form-control">
							<option value="0"><?php echo $this->lang->line('pages_empty_version'); ?></option>
							<?php foreach ($versions as $key => $version): ?>
								<option value="<?php echo $version['version_number'] ?>"><?php echo $this->lang->line('pages_page_version'); ?>: <?php echo $version['version_number'] ?></option>
							<?php endforeach ?>
						</select>
					</div>
					<div class="form-group">
						<label class="control-label"><?php echo $this->lang->line('pages_page_version_comment'); ?></label>
						<textarea class="form-control" rows="2" name="version_comment"></textarea>
					</div>
				</div>
				<div class="modal-footer">
						<button type="button" data-dismiss="modal" class="btn default"><?php echo $this->lang->line('general_cancel') ?></button>
						<button type="submit" class="btn green"><?php echo $this->lang->line('general_done') ?></button>
				</div>
			</div>
			<!-- /.modal-content -->
		</form>
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

