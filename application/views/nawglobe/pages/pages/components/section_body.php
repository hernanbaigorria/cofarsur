<div class="table-responsive">
	<table class="table table-striped table-bordered table-advance table-hover table-vertical-align table-condensed margin-none">
		<thead>
			<tr>
				<th class="text-left">	<?php echo $this->lang->line('pages_page') ?>	</th>
				<th class="text-center" style="width: 180px;">	<?php echo $this->lang->line('pages_status') ?>	</th>
				<th class="text-center" style="width: 140px;">	Options	</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($pages as $key => $_page): ?>
			<?php if (check_permissions('pages', $_page['id_page'])): ?>
				<tr>
					<td>
						<strong><?php echo $_page['page_title'] ?></strong><br>
						<small><a target="_blank" href="<?php echo base_url().$section_slug.'/'.$_page['page_slug'] ?>"><?php echo base_url().$section_slug.'/'.$_page['page_slug'] ?></a></small>
					</td>
					<td class="text-center">
						<?php if (empty($_page['published_version'])): ?>
								<small class="text-danger">No Publicado</small>
							<?php else: ?>
								<small class="text-success">Publicada</small>
						<?php endif ?>
					</td>
					<td class="text-center">
						<div class="btn-group btn-group-solid">
							<?php if ($_page['published_version'] !== FALSE): ?>
							<a title="Editor visual" class="btn btn-xs green" target="_blank" href="<?php echo base_url().GESTORP_MANAGER.'/page_edit' ?>?page=<?php echo $_page['id_page']; ?>&version=<?php echo $_page['published_version']; ?>"><i class="fa fa-pencil"></i></a>
							<?php endif ?>
							<a title="Personalizar" class="btn btn-xs blue" href="?action=edit&id=<?php echo $_page['id_page']; ?>"><i class="fa fa-cog"></i></a>
							<a class="btn btn-xs red red-stripe btn-remove-page margin-none" href="javascript:;" data-id_page="<?php echo $_page['id_page'] ?>" data-confirmation="<?php echo $this->lang->line('pages_delete_sure') ?>"><i class="fa fa-trash-o"></i></a>
						</div>
					</td>
				</tr>
			<?php endif ?>
			<?php endforeach ?>
		</tbody>
	</table>
</div>