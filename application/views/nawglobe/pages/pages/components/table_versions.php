<div class="table-responsive">
	<table class="table table-bordered table-advance table-hover table-vertical-align table-condensed margin-none">
		<thead>
			<tr>
				<th class="text-center" style="width: 65px;">
					<?php echo $this->lang->line('pages_page_version') ?>
				</th>
				<th class="text-center" style="width: 65px;">
					<?php echo $this->lang->line('pages_page_verion_creation_date') ?>
				</th>
				<th class="text-center" style="width: 80px;">
					<?php echo $this->lang->line('pages_status') ?>
				</th>
				<th class="" >
					<?php echo $this->lang->line('pages_page_version_comment') ?>
				</th>
				<th class="" style="width: 200px;">
					<a class="btn btn-xs yellow pull-right btn-create-version" href="javascript:;" data-page="<?php echo $page['id_page']; ?>" >
						<?php echo $this->lang->line('pages_page_new_version') ?>
					</a>
				</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($versions as $key => $version): ?>
			<tr class="<?php if ($page['published_version'] == $version['version_number']) echo 'info' ?>">
				<td class="text-center"><?php echo $version['version_number'] ?></td>
				<td class="text-center"><?php echo date_to_view($version['date_creation']) ?></td>
				<td class="text-center">
					<?php if ($version['published']): ?>
						<small class="text-white"><strong><?php echo $this->lang->line('pages_status_online') ?></strong></small>
					<?php else: ?>
						<small><?php echo $this->lang->line('pages_status_draft'); ?></small>
					<?php endif ?>
				</td>
					
				<td class="">
					<?php if (!empty($version['version_comment'])): ?>
						<?php echo $version['version_comment'] ?>
						<br>
					<?php endif ?>

					<?php $modifier = get_user_name($version['version_last_modifier']); ?>
					<?php $modified = get_time_ago($version['version_last_modification']); ?>
					<?php if (!empty($modifier) AND !empty($modified) AND FALSE): ?>
						<small>Última modificación hace <?php echo $modified ?> por <?php echo $modifier ?></small>
					<?php endif ?>
				</td>

				<td class="text-center">
					<div class="btn-group btn-group-solid">
						<?php if ($page['published_version'] !== $version['version_number']): ?>
							<a class="btn btn-xs grey btn-publish-version" href="javascript:;" data-page="<?php echo $page['id_page']; ?>" data-version="<?php echo $version['version_number'] ?>" data-confirmation="<?php echo $this->lang->line('pages_publish_version_sure') ?>">
								<?php echo $this->lang->line('pages_publish_version') ?>
							</a>
						<?php endif ?>

						<?php if ($page['published_version'] !== $version['version_number']): ?>
							<a class="btn btn-xs green" href="<?php echo base_url().GESTORP_MANAGER.'/page_edit' ?>?page=<?php echo $page['id_page']; ?>&version=<?php echo $version['version_number'] ?>" target="_blank">
								<i class="fa fa-pencil"></i>
							</a>
						<?php else: ?>
							<a class="btn btn-xs yellow" href="<?php echo base_url().GESTORP_MANAGER.'/page_edit' ?>?page=<?php echo $page['id_page']; ?>&version=<?php echo $version['version_number'] ?>" target="_blank">
								<i class="fa fa-pencil"></i>
							</a>
						<?php endif ?>

						<?php if (!$version['published']): ?>
						<a class="btn btn-xs red red-stripe btn-remove-version margin-none" href="javascript:;" data-page="<?php echo $page['id_page']; ?>" data-version="<?php echo $version['version_number'] ?>" data-confirmation="<?php echo $this->lang->line('pages_remove_version_sure') ?>">
							<i class="fa fa-trash-o"></i>
						</a>
						<?php endif ?>
					</div>
				</td>
			</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</div>
