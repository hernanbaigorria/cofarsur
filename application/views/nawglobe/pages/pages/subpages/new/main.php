<div class="tabbable tabbable-custom tabbable-noborder ">
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#tab-page-general" data-toggle="tab" aria-expanded="false">
				<?php echo $this->lang->line('pages_new_page'); ?>
			</a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab-page-general">
			<div class="row">
				<div class="col-md-12">
					<div class="tab-conten innerAll">
						<div id="tab-page-info" class="tab-pane active">
							<form id="frm_create_page" role="form" action="javascript:;">
								
								<p class="<?php if (in_array($preset, array('simple', 'external'))) echo 'hidden' ?>"><?php echo $this->lang->line('pages_wich_page_type'); ?></p>
								<div class="row">
									<div class="col-md-12 padding-none">
										<div class="form-group">
											<div class="radio-list">
												<label class=" col-md-3 <?php if (in_array($preset, array('simple', 'grouped', 'external'))) echo 'hidden' ?>">
													<input type="radio" name="page_type" id="page_type6" value="page" <?php if ($preset == 'simple') echo 'checked' ?>> 
													<?php echo $this->lang->line('pages_page') ?>  <br>
												</label>
												<label class=" col-md-3 <?php if (in_array($preset, array('simple', 'external'))) echo 'hidden' ?>">
													<input type="radio" name="page_type" id="page_type4" value="section" <?php if ($preset == 'grouped' AND empty($parent)) echo 'checked' ?>> 
													<?php echo $this->lang->line('pages_section') ?>  <br>
													
												</label>
												<label class=" col-md-3 <?php if (in_array($preset, array('simple', 'external'))) echo 'hidden' ?>">
													<input type="radio" name="page_type" id="page_type5" value="section_page" <?php if ($preset == 'grouped' AND !empty($parent)) echo 'checked' ?>> 
													<?php echo $this->lang->line('pages_section_page') ?> <br>
												</label>
												<label class=" col-md-3 <?php if (in_array($preset, array('simple', 'grouped', 'external'))) echo 'hidden' ?>">
													<input type="radio" name="page_type" id="page_type5" value="external_page" <?php if ($preset == 'external') echo 'checked' ?>> 
													<?php echo $this->lang->line('pages_external_pages') ?><br>
												</label>
											</div>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label"><?php echo $this->lang->line('pages_page_name'); ?></label>
									<input required name="page_title" type="text" placeholder="<?php echo $this->lang->line('general_write_here'); ?>" class="form-control">
								</div>

								<div class="form-group hidden-external hidden-section hidden-simple-page hidden">
									<label class="control-label"><?php echo $this->lang->line('pages_page_parent_section'); ?></label>
									<select name="page_parent" class="form-control">
										<option value=""><?php echo $this->lang->line('pages_section'); ?></option>
										<?php foreach ($sections as $key => $section): ?>
											<option <?php if ($parent == $section['id_page']) echo 'selected' ?> value="<?php echo $section['id_page'] ?>" data-slug="<?php echo $section['page_slug'] ?>/"><?php echo $section['page_title'] ?></option>
										<?php endforeach ?>
									</select>
								</div>

								<div class="form-group">
									<label><?php echo $this->lang->line('pages_page_slug'); ?></label>
									<div class="input-group">
										<span class="input-group-addon ">
											<div class="hidden-external"><?php echo base_url() ?><span id="spn-section-uri" class="hidden-section hidden-simple-page hidden"></span></div>
											<div class="hidden-section-page hidden-section hidden-simple-page hidden">URL</div>
										</span>
										<input name="page_slug" type="text" class="form-control input-slug hidden-external" placeholder="<?php echo $this->lang->line('general_write_here'); ?>">
										<input name="page_url" type="text" class="form-control hidden-section-page hidden-section hidden-simple-page hidden" placeholder="<?php echo $this->lang->line('general_write_here'); ?>">
										<span class="input-group-addon hidden-external hidden-section-page hidden-simple-page hidden">
											<?php echo $this->lang->line('pages_spage_uri'); ?>
										</span>
									</div>
								</div>

								<div class="form-group hidden-external hidden-section-page">
									<label class="control-label"><?php echo $this->lang->line('pages_page_layout'); ?></label>
									<select name="page_layout" class="form-control">
										<?php foreach ($layouts as $key => $layout): ?>
											<option><?php echo $layout ?></option>
										<?php endforeach ?>
									</select>
								</div>
								
								<div class="margin-top-10">
									<input type="hidden" name="preset" value="<?php echo $preset ?>">
									<a href="<?php echo base_url().GESTORP_MANAGER ?>/pages" class="btn default">
										<?php echo $this->lang->line('general_cancel'); ?> 
									</a>
									<button type="submit" class="btn green">
										<?php echo $this->lang->line('general_save'); ?> 
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>