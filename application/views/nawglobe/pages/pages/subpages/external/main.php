<div class="table-responsive">
	<table class="table table-striped table-bordered table-advance table-hover table-vertical-align table-condensed margin-none">
		<thead>
			<tr>
				<th class="text-left">	<?php echo $this->lang->line('pages_page') ?>	</th>
				<th class="text-center" style="width: 140px;">	Options	</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($pages as $key => $_page): ?>
			<?php if (check_permissions('pages', $_page['id_page'])): ?>
				<tr>
					<td>
						<strong><?php echo $_page['page_title'] ?></strong><br>
						<small><a target="_blank" href="<?php echo base_url().$_page['page_slug'] ?>"><?php echo base_url().$_page['page_slug'] ?></a></small>
					</td>
					<td class="text-center">
						<div class="btn-group btn-group-solid">
							<a title="Personalizar" class="btn btn-xs blue" href="?action=edit&id=<?php echo $_page['id_page']; ?>"><i class="fa fa-cog"></i></a>
							<a class="btn btn-xs red red-stripe btn-remove-page margin-none" href="javascript:;" data-id_page="<?php echo $_page['id_page'] ?>" data-confirmation="<?php echo $this->lang->line('pages_delete_sure') ?>"><i class="fa fa-trash-o"></i></a>
						</div>
					</td>
				</tr>
			<?php endif ?>
			<?php endforeach ?>
		</tbody>
	</table>
</div>