<!-- /.modal -->
<div class="modal fade bs-modal-md" id="modal-update-picture" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><?php echo $this->lang->line('users_update_profile_picture'); ?></h4>
			</div>
			<div class="modal-body">
				<form role="form" id="frm-update-profile-picture">
					<input type="hidden" name="id_user" value="<?php echo $id_user ?>">
					<div class="innerAll form-group form-md-line-input">
						<div class="row">
							<div class="fileinput fileinput-new profile-picture-updater" data-provides="fileinput">
								<div class="col-md-4">
									<div>
										<span class="btn default btn-block btn-file" style="margin: 5px;">
											<span class="fileinput-new"> <?php echo $this->lang->line('users_change_picture'); ?> </span>
											<span class="fileinput-exists">	<?php echo $this->lang->line('users_pick_another'); ?> </span>
											<input type="file" name="profile_picture">
										</span>
										<a href="javascript:;" class="btn default btn-block fileinput-exists" data-dismiss="fileinput" style="margin: 5px;"> <?php echo $this->lang->line('general_cancel'); ?> </a>
										<button type="send" class="btn blue btn-block fileinput-exists" style="margin: 5px;"> <?php echo $this->lang->line('users_upload_picture'); ?></button>										<p class="fileinput-exists"></p>
									</div>
								</div>
								<div class="col-md-8 text-center" id="profile-image-holder">
									<div class="fileinput-new "  style="width: 100%; max-height: 250px;">
										<img id="croppable_img" src="<?php echo get_avatar($id_user); ?>" width="250"/>
									</div>
									<div class="fileinput-preview fileinput-exists " style="width: 100%; max-height: 250px;">
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

