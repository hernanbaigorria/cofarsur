<?php $can_manage_permissions = (check_permissions('gestorp', 'manage_permissions') AND ($this->session->userdata('id_user') !== $user['id_user'])); ?>
<div class="tabbable tabbable-custom tabbable-noborder ">
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#tab-user-general" data-toggle="tab" aria-expanded="false">
				<?php echo $this->lang->line('general_account_configuration'); ?>
			</a>
		</li>
		<?php if ($can_manage_permissions): ?>
		<li class="">
			<a href="#tab-permissions" data-toggle="tab" aria-expanded="true">
				<?php echo $this->lang->line('users_permissions'); ?>
			</a>
		</li>			
		<?php endif ?>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab-user-general">
			<div class="row">
				<div class="col-md-3">
					<ul class="list-unstyled profile-nav">
						<li>
							<img src="<?php echo get_avatar($user['id_user']) ?>" class="img-responsive" alt="">
							<a href="javascript:;" data-id_user="<?php echo $user['id_user'] ?>" class="profile-edit btn-change-picture">
								<?php echo $this->lang->line('general_edit'); ?>
							</a>
						</li>
						<li>
							<a data-toggle="tab" href="#tab-user-info" aria-expanded="false">
								<?php echo $this->lang->line('general_account_configuration'); ?>
							</a>
						</li>
						<li>
							<a data-toggle="tab" href="#tab-change-password" aria-expanded="false">
								<?php echo $this->lang->line('general_change_password'); ?>
							</a>
						</li>
					</ul>
				</div>
				<div class="col-md-9">
					<div class="tab-content">
						<div id="tab-user-info" class="tab-pane active">
							<form id="frm_update_account" role="form" action="javascript:;">
								<div class="form-group">
									<label class="control-label"><?php echo $this->lang->line('users_name'); ?></label>
									<input required name="user_name" type="text" placeholder="" value="<?php echo $user['user_name'] ?>" class="form-control">
								</div>
								<div class="form-group">
									<label class="control-label"><?php echo $this->lang->line('users_email'); ?></label>
									<input required name="user_email" type="email" placeholder="" value="<?php echo $user['user_email'] ?>" class="form-control">
								</div>
								<div class="form-group">
									<label class="control-label"><?php echo $this->lang->line('users_language'); ?></label>
									<select name="user_language" class="form-control">
										<option <?php if ($user['user_language'] == 'english') echo 'selected' ?> value="english">English</option>
										<option <?php if ($user['user_language'] == 'spanish') echo 'selected' ?> value="spanish">Spanish</option>
									</select>
								</div>
								<input type="hidden" name="id_user" value="<?php echo $user['id_user'] ?>">
								<div class="margin-top-10">
									<button type="submit" class="btn green">
										<?php echo $this->lang->line('general_save'); ?> 
									</button>
								</div>
							</form>
						</div>
						<div id="tab-change-password" class="tab-pane ">
							<form id="frm_update_password" action="javascript:;">
								<div class="form-group">
									<label class="control-label"><?php echo $this->lang->line('users_new_password'); ?></label>
									<input name="user_password" type="password" class="form-control">
								</div>
								<div class="form-group">
									<label class="control-label"><?php echo $this->lang->line('users_r_new_password'); ?></label>
									<input name="user_r_password" type="password" class="form-control">
								</div>
								<input type="hidden" name="id_user" value="<?php echo $user['id_user'] ?>">
								<div class="margin-top-10">
									<button type="submit" class="btn green">
										<?php echo $this->lang->line('general_save'); ?> 
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php if ($can_manage_permissions): ?>
		<div class="tab-pane " id="tab-permissions">
			<?php load_page_piece('form_manage_permissions'); ?>
		</div>
		<?php endif ?>
	</div>
</div>