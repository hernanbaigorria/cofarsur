<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *  Configuracion de Permisos
 *
 *  $config[permissions][PERMISION_GROUP][PERMISSION_ITEM] = PERMISSION_DEFAULT_VALUE
 *
 *  Asegurarse que el PERMISSION_ITEM tenga su linea de traduccion en general_lang, de lo
 *  contrario en las pantallas de configuracion se vera el rotulo en blanco.
 *
 *  PERMISSION_DEFAULT_VALUE puede ser FALSE o FALSE
 */

// Permisos para menus y pantallas (override gestorp defaults)

	// Permiso global para usuarios.
	$config['permissions']['gestorp']['manage_users'] 			= TRUE;
	$config['permissions']['gestorp']['manage_permissions'] 	= TRUE;
	
	// Permiso global para modulos.
	$config['permissions']['gestorp']['manage_modules'] 		= TRUE;

	// Permiso global para paginas.
	$config['permissions']['gestorp']['manage_pages'] 			= TRUE;
	$config['permissions']['gestorp']['manage_create_pages'] 	= TRUE;
	
	// Permiso para manejar la configuracion general del sitio.
	$config['permissions']['gestorp']['manage_configurations'] 	= FALSE;

	// Permisos para multimedia.
	$config['permissions']['gestorp']['manage_media'] 	= FALSE;
	$config['permissions']['gestorp']['upload_files'] 	= FALSE;
	$config['permissions']['gestorp']['edit_files'] 	= FALSE;
	$config['permissions']['gestorp']['remove_files'] 	= FALSE;
	$config['permissions']['gestorp']['replace_files'] 	= FALSE;

// Permisos custom del tema.

	$config['permissions']['mod_paquetes']['publish_paquetes'] 	= FALSE;