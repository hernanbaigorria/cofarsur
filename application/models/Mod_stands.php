<?php 
class Mod_stands extends CI_Model 
{
	
	public function __construct() 
	{

	}

	public function get_stands()
	{
		$result = $this->db->get('mod_productos_categories');
		return $result->result();
	}

	public function insert_interaccion($data)
	{
		$result = $this->db->insert('interacciones', $data);
		if ($result == TRUE) 
            return $this->db->insert_id();
        else
            return FALSE; 

	}

}