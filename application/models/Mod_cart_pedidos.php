<?php 
class Mod_cart_pedidos extends CI_Model 
{
	
	public function __construct() 
	{

	}

	public function insert_cart($data)
	{
		$result = $this->db->insert('carrito', $data);

        if ($result == TRUE) 
            return $this->db->insert_id();
        else
            return FALSE; 
	}

	public function delete_cart($data)
	{

		$this->db->where('id_user', $data);
		$this->db->delete('carrito');
	}

	public function insert_pedido($data)
	{
		$result = $this->db->insert('pedidos', $data);

        if ($result == TRUE) 
            return $this->db->insert_id();
        else
            return FALSE; 
	}

	public function update($data){

		$result = $this->db->update('carrito', $data);

		if ($result == TRUE) 
		    return $this->db->insert_id();
		else
		    return FALSE;
	}

	public function get_carrito_user($id_user, $count = FALSE)
	{
		if ($id_user === FALSE) return FALSE;
		$cond['id_user'] = $id_user;

		$this->db->select('SQL_CALC_FOUND_ROWS carrito.*,  mod_productos.*, mod_productos_categories.*', FALSE);
		$this->db->join('mod_productos', 'mod_productos.id_product = carrito.id_producto');
		$this->db->join('mod_productos_categories', 'mod_productos_categories.id_category = carrito.id_stand');
		$this->db->from('carrito');
		$this->db->where($cond);
		$result = $this->db->get();
		if($count == true):
			return $result->num_rows();
		else:
			if ($result->num_rows() > 0)
			{
				$result = $result->result_array();
				return $result;
			} else {
				$result = FALSE;
			}
		endif;

		return FALSE;
	}


	public function get_pedidos_stand($id_stand, $count = FALSE)
	{
		if ($id_user === FALSE) return FALSE;
		$cond['id_stand'] = $id_stand;

		$this->db->select('SQL_CALC_FOUND_ROWS pedidos.*,  mod_productos.*, mod_productos_categories.*', FALSE);
		$this->db->join('mod_productos', 'mod_productos.id_product = pedidos.id_producto');
		$this->db->join('mod_productos_categories', 'mod_productos_categories.id_category = pedidos.id_stand');
		$this->db->from('pedidos');
		$this->db->where($cond);
		$result = $this->db->get();
		if($count == true):
			return $result->num_rows();
		else:
			if ($result->num_rows() > 0)
			{
				$result = $result->result_array();
				return $result;
			} else {
				$result = FALSE;
			}
		endif;

		return FALSE;
	}

	public function get_ult_num_pedido()
	{
		$this->db->select('n_pedido	');
		$this->db->order_by('id_pedido',"desc");
		$this->db->limit(1);
		$result = $this->db->get('pedidos');
		return $result->result();
	}

	public function delet_cart_product($id_user, $id_product){
		$this->db->where('id_user', $id_user);
		$this->db->where('id_producto', $id_product);

		$result = $this->db->delete('carrito');

		return $this->db->affected_rows();
	}

	public function actualizar_stock($id_producto, $stock_pedido)
	{

		$this->db->where('id_product', $id_producto);
		$this->db->select('product_stock');
		$result = $this->db->get('mod_productos');
		$stock = $result->result();

		// ACTUALIZAR STOCK
		$resta = $stock[0]->product_stock - $stock_pedido; 

		$this->db->where('id_product', $id_producto);
		$this->db->set('product_stock', $resta);
		$this->db->update('mod_productos');
	}
}