<?php
class Storage extends CI_Model 
{

    public function __construct() {}

    // Gestion de uploads

        // Carga un archivo al server
        // Return: Array on success, False on error.
        function upload_file($filed_name, $upload_path, $max_size = 4096, $encrypt_name = FALSE)
        {
            // Si el directorio no existe, lo creamos.
            if(!is_dir($upload_path)) mkdir($upload_path, 0777);            
            
            // Inicializamos la libreria para upload.
            $config['upload_path'] = $upload_path;
            $config['allowed_types'] = 'bmp|BMP|doc|docx|odt|pages|rtf|tex|txt|wpd|wps|csv|dat|gbr|ged|key|pps|ppt|pptx|sdf|tar|vcf|xml|aif|iff|m3u|m4a|mid|mp3|mpa|ra|wav|wma|3g2|3gp|asf|asx|avi|flv|m4v|mov|mp4|mpg|rm|srt|swf|vob|wmv|3dm|3ds|max|obj|bmp|dds|gif|jpg|png|psd|tga|thm|tif|tiff|yuv|ai|eps|ps|svg|indd|pct|pdf|xlr|xls|xlsx|dwg|dxf|7z|cbr|deb|gz|pkg|rar|rpm|sitx|tar.gz|zip|zipx|DOC|DOCX|ODT|PAGES|RTF|TEX|TXT|WPD|WPS|CSV|DAT|GBR|GED|KEY|PPS|PPT|PPTX|SDF|TAR|VCF|XML|AIF|IFF|M3U|M4A|MID|MP3|MPA|RA|WAV|WMA|3G2|3GP|ASF|ASX|AVI|FLV|M4V|MOV|MP4|MPG|RM|SRT|SWF|VOB|WMV|3DM|3DS|MAX|OBJ|BMP|DDS|GIF|JPG|PNG|PSD|TGA|THM|TIF|TIFF|YUV|AI|EPS|PS|SVG|INDD|PCT|PDF|XLR|XLS|XLSX|DWG|DXF|7Z|CBR|DEB|GZ|PKG|RAR|RPM|SITX|ZIP|ZIPX';                  
            $config['max_size'] = $max_size;
            $config['encrypt_name'] = $encrypt_name;
            $config['remove_spaces'] = TRUE;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            // Subimos el Archivo.
            $result = $this->upload->do_upload($field_name);

            if ($result == TRUE)
                return $this->upload->data();
            else
                return FALSE;
        }

        // Carga una imagen al server
        // Return: Array on success, False on error.
        function upload_picture($field_name, $upload_path, $max_size = 4096, $encrypt_name = FALSE, $file_name = FALSE)
        {
            // Si el directorio no existe, lo creamos.
            if(!is_dir($upload_path)) mkdir($upload_path, 0777);    

            // Si forzamos un nombre final, lo establecemos.
            if ($file_name !== FALSE) $config['file_name'] = $filename;

            // Inicializamos la libreria para upload.
            $config['upload_path'] = $upload_path;
            $config['allowed_types'] = 'bmp|BMP|jpg|png|gif|JPG|PNG|GIF|JPEG|jpeg';
            $config['max_size'] = $max_size;
            $config['encrypt_name'] = $encrypt_name;
            $config['remove_spaces'] = TRUE;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            // Subimos el Archivo.
            $result = $this->upload->do_upload($field_name);

            if ($result == TRUE)
                return $this->upload->data();

            return FALSE;    
        }

        // Crea una copia de la imagen, y la redimensiona. Si no se especifica ruta de destino sobrescribe la original.
        // Return: True on success, False on error.
        function resize_picture($source_file, $destination_path = FALSE, $max_width = 512, $max_height = 512, $maintain_ratio = TRUE)
        {
            $this->load->library('image_lib'); 
            
            $config['image_library'] = 'gd2';
            $config['source_image'] = $source_file;
            if ($destination_path !== FALSE) $config['new_image'] = $destination_path;
            $config['create_thumb'] = FALSE;
            $config['maintain_ratio'] = $maintain_ratio;
            $config['width'] = $max_width;
            $config['height'] = $max_height;            
            
            $this->image_lib->initialize($config);
            return $this->image_lib->resize();
        }

        // Crea una copia de  la imagen, y la corta. Si no se espcifica ruta de destino reemplaza la original.
        // Return: Ture on succes, False on error.
        function crop_picture($source_file, $destination_path = FALSE, $x_start = 0, $y_start = 0, $width = 1, $height = 1)
        {
            $this->load->library('image_lib'); 
            
            $config['image_library'] = 'gd2';
            $config['source_image'] = $source_file;
            if ($destination_path !== FALSE) $config['new_image'] = $destination_path;
            $config['maintain_ratio'] = FALSE;
            $config['x_axis'] = $x_start;
            $config['y_axis'] = $y_start;
            $config['width'] = $width;
            $config['height'] = $height;            
            
            $this->image_lib->initialize($config);
            $this->image_lib->crop();
            echo $this->image_lib->display_errors();
        }

}
