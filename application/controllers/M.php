<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M extends MX_Controller {

	public function index()
	{
		// Identificamos el primer segmento de  URI, y vemos si pertenece a alguna Seccion o pagina.
		$id_media 	= (int)$this->uri->segment(2);	// ID de recurso.
		$media_item = $this->media->get_file($id_media);

		if ($media_item === FALSE)
			exit();

		redirect(UPLOADS_URL.$media_item['media_file_name']);
	}
	
}
