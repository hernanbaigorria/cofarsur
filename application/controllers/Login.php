<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {

	function __construct() {
	    parent::__construct();
	    $this->load->model('mod_stands');
	}

	public function index()
	{	

		$stands = $this->mod_stands->get_stands();

		//Create the client object
		$soapclient = new SoapClient('http://www.cofarsur.net/ws?token=R9RHXSq6yLQtXqsKHK0e1PtQHbzX6Ul9&wsdl');

		//Use the functions of the client, the params of the function are in 
		//the associative array
		$params = array('usuario' => $_POST['usuario'], 'clave' => $_POST['clave'], 'token' => 'R9RHXSq6yLQtXqsKHK0e1PtQHbzX6Ul9');
		$response = $soapclient->ConsultarDatosCliente($params);

		$array = (json_decode(json_encode($response), true));


		if(!isset($array['error'])){

			$this->load->helper('cookie');

			$name   = 'id_user';
			$value  = $array['clienteId'];
			$expire = time()+1000;
			$path  = '/';
			$secure = TRUE;

			setcookie($name,$value,$expire,$path);

			$name_2   = 'clienteNombre';
			$value_2  = $array['clienteNombre'];
			$expire_2 = time()+1000;
			$path_2  = '/';
			$secure_2 = TRUE;

			setcookie($name_2,$value_2,$expire_2,$path_2);

			$name_3   = 'clienteTipo';
			$value_3  = $array['clienteTipo'];
			$expire_3 = time()+1000;
			$path_3  = '/';
			$secure_3 = TRUE;

			setcookie($name_3,$value_3,$expire_3,$path_3);

			foreach($stands as $key => $stand){

				$this->db->where('id_user', $array['clienteId']);
				$this->db->where('id_stand', $stand->id_category);
				$query = $this->db->get('interacciones');

				if ( $query->num_rows() == 0 ) {
					$data_insert = array(
						'id_user' => $array['clienteId'],
						'id_stand' => $stand->id_category
					);

					$this->mod_stands->insert_interaccion($data_insert);
				}
			}


			//$name   = 'tipo_cliente';
			//$value  = $array['clienteTipo'];
			//$expire = time()+1000;
			//$path  = '/';
			//$secure = TRUE;

			//setcookie($name,$value,$expire,$path); 

			echo "userok";
			exit;
		} else {
			echo "nouser";
			exit;
		}
		
	}

	public function loginout()
	{
		$this->load->helper('cookie');
		delete_cookie('id_user');
		redirect(base_url());
	}

	public function salir_manager()
	{	
		$this->session->sess_destroy();
		redirect(base_url().GESTORP_MANAGER);

	}

	
}
