<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_users extends CI_Controller {

    function __construct() {
        parent::__construct();
        
        if(!$this->input->is_ajax_request()) 
			show_404();
    }

	public function iniciar_sesion()
	{
		$this->lang->load('login_lang', DEFAULT_LANGUAGE);

		$email 			= $this->input->post('email', TRUE);
		$password 		= $this->input->post('password', TRUE);

		// Comprobamos las credenciales del usuario.
		$result = $this->users->login($email, md5($password));

		// Si las credenciales son invalidas
		if ($result == FALSE)
			ajax_response('danger', $this->lang->line('login_incorrect_email_or_p'), '0', '1', NULL);
		else
		{
			// Sino inicializamos la sesion.
			$result['logged_in'] = TRUE;
			$result['token'] = generate_session_token($result['id_user'], $result['user_email']);
			$this->session->set_userdata($result);


			// Y redirigimos el usuario a donde estaba.
			$ext['action'] = 'redirect';
			$ext['redirect_delay'] 	= '10';
			$ext['redirect_target'] = base_url().GESTORP_MANAGER;
			ajax_response('success', $this->lang->line('general_everything_ok'), '100', '0', $ext);
		}
	}

	public function cerrar_sesion()
	{

		print_r("hola");
		exit;
		$this->session->sess_destroy();

		redirect(base_url().GESTORP_MANAGER);
		exit;

		$ext['action'] = 'redirect';
		$ext['redirect_delay'] 	= '10';
		$ext['redirect_target'] = base_url().GESTORP_MANAGER;
		ajax_response('success', $this->lang->line('general_everything_ok'), '100', '0', $ext);
	}

	public function recuperar_clave() 
	{
		$this->lang->load('login_lang', DEFAULT_LANGUAGE);
		$usr_email = $this->input->post('email');
		
		// Verificamos que el usuario exista en el sistema.
			$data['user'] = $this->users->get_user_by('user_email', $usr_email);
			if ($data['user'] == FALSE) {
				echo ajax_response('danger', $this->lang->line('login_user_no_exist'), 0, 1);
				exit();
			}

		// Generamos la clave temporal
			$password = $this->users->gen_temporal_password($data['user']['id_user']);
			if ($password == FALSE) {
				echo ajax_response('danger', $this->lang->line('login_temp_password_problem'), 0, 1);
				exit();
			}

		// Le despachamos el email con la clave
			$this->load->model('notifications');
			$this->notifications->new_temporal_password($data['user']['id_user'], $password);

		echo ajax_response('success', $this->lang->line('login_temp_password_ok'), '1', '1', NULL);
	}

	public function create_account()
	{
		$user_name			= $this->input->post('user_name');
		$user_email			= $this->input->post('user_email');
		$user_language		= $this->input->post('user_language');
		$user_password		= $this->input->post('user_password');
		$user_r_password	= $this->input->post('user_r_password');

		// Controlamos que el usuario tenga permiso de gestionar usuarios
		check_permissions('gestorp', 'manage_users', FALSE, TRUE);

		if ($this->users->chk_email($user_email) != FALSE)
			ajax_response('danger', $this->lang->line('general_email_used'), '0', '1');

		if ($user_password != $user_r_password)
			ajax_response('danger', $this->lang->line('general_pwn_match_error'), '0', '1');

		$id_user = $this->users->new_account($user_name, $user_email, $user_language, md5($user_password));

		if ($id_user !== FALSE)
		{
			$ext['action'] = 'redirect';
			$ext['redirect_delay'] 	= '500';
			$ext['redirect_target'] = base_url().GESTORP_MANAGER.'/users?action=edit&id='.$id_user;
			ajax_response('success', $this->lang->line('users_created_succesfully'), '1', '1', $ext);
		}
		else
			ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
	}

	public function update_account()
	{
		$id_user			= $this->input->post('id_user');
		$user_name			= $this->input->post('user_name');
		$user_email			= $this->input->post('user_email');
		$user_language		= $this->input->post('user_language');

		// Controlamos que el usuario tenga permiso de gestionar usuarios
		if ($this->session->userdata('id_user') !== $id_user)
			check_permissions('gestorp', 'manage_users', FALSE, TRUE);

		if ($this->users->chk_email($user_email, $id_user) != FALSE)
			ajax_response('danger', $this->lang->line('general_email_used'), '0', '1');

		// Realizamos el update.
		$errors = 0;
		$errors += (int)!$this->users->set_account($id_user, 'user_name', $user_name);
		$errors += (int)!$this->users->set_account($id_user, 'user_email', $user_email);
		$errors += (int)!$this->users->set_account($id_user, 'user_language', $user_language);

		if ($errors == 0)
		{
			$ext['action'] 			= 'reload';
			$ext['reload_delay'] 	= '500';
			ajax_response('success', $this->lang->line('general_msg_saved_ok'), '1', '1', $ext);
		}
		else
			ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
	}

	public function remove_account()
	{
		$id_user			= $this->input->post('id_user');

		// Controlamos que el usuario tenga permiso de gestionar usuarios
		check_permissions('gestorp', 'manage_users', FALSE, TRUE);
		
		if ($this->session->userdata('id_user') == $id_user) // No se puede borrar a si mismo
			ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');

		// Realizamos el update.
		$result = $this->users->del_account($id_user);

		if ($result == TRUE)
		{
			$ext['action'] 			= 'reload';
			$ext['reload_delay'] 	= '500';
			ajax_response('success', $this->lang->line('users_removed_succesfully'), '1', '1', $ext);
		}
		else
			ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
	}

	public function update_password()
	{
		$id_user			= $this->input->post('id_user');
		$user_password		= $this->input->post('user_password');
		$user_r_password	= $this->input->post('user_r_password');

		if ($user_password != $user_r_password)
			ajax_response('danger', $this->lang->line('general_pwn_match_error'), '0', '1');

		// Controlamos que el usuario tenga permiso de gestionar usuarios
		if ($this->session->userdata('id_user') !== $id_user)
			check_permissions('gestorp', 'manage_users', FALSE, TRUE);

		$result = $this->users->set_account($id_user, 'user_password', md5($user_password));

		if ($result == TRUE)
			ajax_response('success', $this->lang->line('general_msg_saved_ok'), '1', '1');
		else
			ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
	}

    public function get_modal_picture()
    {
    	$id_user = $this->input->post('id_user');
    	
    	$this->lang->load('users_lang', $this->session->userdata('user_language'));
		
		// Controlamos que el usuario tenga permiso de gestionar usuarios
		if ($this->session->userdata('id_user') !== $id_user)
			check_permissions('gestorp', 'manage_users', FALSE, TRUE);

		$data['id_user'] = $id_user;

		// Armamos la respuesta en JSON
		$ext['composer'] = $this->load->view('nawglobe/pages/users/components/modal_update_profile_picture', $data, TRUE);	
		echo ajax_response('success', 'Se puede realizar el cambio de imagen.', 1, 0, $ext);
    }

	public function update_profile_picture()
	{
		$id_user 		= $this->input->post('id_user');
		$new_picture 	= 'profile_picture';

		if ($this->session->userdata('id_user') !== $id_user)
			check_permissions('gestorp', 'manage_users', FALSE, TRUE);

		// Si recibimos un archivo, seguramente es porque hay que actualizar la foto de perfil. Y no cropear la existente.
		if (isset($_FILES['profile_picture']))
		{
			$this->load->model('storage');
			$upload_result = $this->storage->upload_picture($new_picture, PATH_UPLOADS.'u/', 4096, TRUE);
			if ($upload_result !== FALSE)
			{
				// Ajustamos el tamaño de la imagen subida, y la conservamos como original.
				$this->storage->resize_picture(PATH_UPLOADS.'u/'.$upload_result['file_name'], FALSE, 512, 512, FALSE);

				// Eliminamos la fotografia de perfil anterior.
				$this->users->del_profile_picture($id_user);
				// Fijamos el nombre de la nueva imagen.
				$result = $this->users->set_user_profile_picture($id_user, $upload_result['file_name']);
				
				if ($result == TRUE)
				{
					$ext['action'] 			= 'reload';
					$ext['reload_delay'] 	= '500';
					ajax_response('success', $this->lang->line('general_msg_saved_ok'), '1', '1', $ext);
				}
				else
					ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
			}
		}
		else
			ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
	}

	public function update_permissions()
	{
		$id_user 			= $this->input->post('id_user');
		$permission_group 	= $this->input->post('permission_group');
		$items_grouped 		= $this->input->post('page_permissions_selector');

		unset($_POST['id_user']);
		unset($_POST['permission_group']);
		unset($_POST['page_permissions_selector']);

		$permissions = $this->input->post();
		
		// Controlamos los permisos establecidos en le jstree.
		if ($permission_group == 'pages') 
		{
			$jstree_permissions = explode(',', $items_grouped);
			foreach ($permissions as $key => $value) {
				if (!in_array($key, $jstree_permissions)) 
					$permissions[$key] = '0';
			}
		}
		
		foreach ($permissions as $permission_item => $value) {
			$this->permissions->set($id_user, $permission_group, $permission_item, (bool)$value);
		}

		ajax_response('success', $this->lang->line('general_msg_saved_ok'), '1', '1');
	}

}
