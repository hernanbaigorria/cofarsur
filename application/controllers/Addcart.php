<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Addcart extends MX_Controller {

	function __construct() {
	    parent::__construct();
	    $this->load->model('mod_cart_pedidos');
	    $this->load->library('excel');
	    $this->load->helper('cookie');
	    $this->lang->load('general_lang', $this->session->userdata('user_language'));
	}

	function _output($output)
    {
    	if(!$this->input->is_ajax_request()) 
        	echo minify_html($output);
        else
        	echo $output;
    }

    public function index()
    {   
        foreach($_POST as $p => $k)
        {
            $id = explode("stockproduct_",$p);
            if(isset($id[1])):
                if($_POST['stockproduct_'.$id[1]] > 0):
                    $data_entry = array(
                        'id_user' => $this->input->cookie('id_user'),  
                        'id_producto' => $id[1],
                        'id_stand' => $this->input->post('id_stand'),
                        'grupo_cart' => $_POST['grupo_'.$id[1]],
                        'unidades' => $_POST['unid_'.$id[1]],
                        'stock_elgido' => $_POST['stockproduct_'.$id[1]]
                    );
                    $result = $this->mod_cart_pedidos->insert_cart($data_entry);
                endif;
            endif;
        }

        $CodIduser = base64_encode($this->input->cookie('id_user'));
        $urlEncode = urlencode($CodIduser);

        if ($result !== FALSE) 
        {
            $ext['action']          = 'redirect';
            $ext['redirect_target'] = base_url().'carrito?u='.$urlEncode.'';
            $ext['redirect_delay']  = '500';
            ajax_response('success', 'Producto eliminado correctamente.', '1', '1', $ext);
        }
        
    }

    public function delete_product()
    {   
        $result = $this->mod_cart_pedidos->delet_cart_product($this->input->post('id_user'), $this->input->post('id_product'));
        if ($result == TRUE) 
        {
            $ext['action']          = 'reload';
            $ext['reload_delay']    = '500';
            ajax_response('success', $this->lang->line('pages_removed_succesfully'), '1', '1', $ext);
        }

        ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
    }

}
