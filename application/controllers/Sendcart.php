<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sendcart extends MX_Controller {

	function __construct() {
	    parent::__construct();
	    $this->load->model('mod_cart_pedidos');
	    $this->load->library('excel');
	    $this->load->helper('cookie');
	    $this->lang->load('general_lang', $this->session->userdata('user_language'));
	}

	function _output($output)
    {
    	if(!$this->input->is_ajax_request()) 
        	echo minify_html($output);
        else
        	echo $output;
    }
    public function index (){

        $CodIduser = base64_encode($_POST['user_id']);
        $urlEncode = urlencode($CodIduser);
        
        $ext['action']          = 'redirect';
        $ext['redirect_target'] = base_url().'solicitud?cart='.$urlEncode.'';
        $ext['redirect_delay']  = '500';
        ajax_response('success', 'Producto eliminado correctamente.', '1', '1', $ext);
    }
    public function savePedido()
    {   

        $listaProductos = $this->mod_cart_pedidos->get_carrito_user($_POST['user_id']);
        $ultPedid = $this->mod_cart_pedidos->get_ult_num_pedido();

        /* Actualizando interaccion de usuario */

        foreach ($listaProductos as $key => $stand) {

            $this->db->where('id_user', $stand['id_user']);
            $this->db->where('id_stand', $stand['id_stand']);
            $this->db->where('estado', 'Ya he comprado');
            $query = $this->db->get('interacciones');

            if ( $query->num_rows() == 0 ) {
                $data_up = array('estado' => 'Ya he comprado');
                $this->db->where('id_user', $stand['id_user']);
                $this->db->where('id_stand', $stand['id_stand']);
                $this->db->update('interacciones', $data_up);
            }
           
        }

        // Nombre de excel final

        $identificador_global = $_POST['user_id'].rand(99, 99999);
        
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);         
        $objPHPExcel->getActiveSheet()->setTitle('Transfer');         
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Nro de Transfer');         
        $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Fecha');         
        $objPHPExcel->getActiveSheet()->setCellValue('C1', 'ID Cliente');         
        $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Razon Social');         
        $objPHPExcel->getActiveSheet()->setCellValue('E1', 'EAN');         
        $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Descripcion');         
        $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Pedido');         
        $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Descuento');
         


        foreach ($listaProductos as $key => $product) {

            // Armando nomenclatura de pedidos 2202001200001  (Sería el primer pedido de Perfumería (2) de este año (2020) del evento Procter (01) en Stand-Laboratorio X (20) pedido nro 0001)

            $contNumber = $key+1;

            if(!empty($ultPedid[0]->n_pedido)){
                $nropedido =  str_pad($ultPedid[0]->n_pedido+$contNumber, 4, 0, STR_PAD_LEFT);
            } else {
                $nropedido = str_pad($contNumber, 4, 0, STR_PAD_LEFT);
            }

            $nomenclatura = $product['tipo_stand'].date("Y").$_GET['nEvent'].$product['id_stand'].$nropedido;

            if($product['unidades'] > 0):
                $pedido = $product['stock_elgido']*$product['unidades'];
            else:
                $pedido = $product['stock_elgido'];
            endif;

            $dataPedido = array(
                'identificador_global' => $identificador_global,
                'n_pedido' => $nropedido,
                'nomenclatura' => $nomenclatura,
                'id_user' => $product['id_user'],
                'razon_social' => $this->input->cookie('clienteNombre'),
                'id_producto' => $product['id_producto'],
                'id_stand' => $product['id_stand'],
                'stock_elgido' => $pedido,
                'fecha_pedido' => date("Y-m-d H:i:s")
            );

            $this->mod_cart_pedidos->actualizar_stock($product['id_producto'], $product['stock_elgido']);

            $id_pedido = $this->mod_cart_pedidos->insert_pedido($dataPedido);
            $number = $key+2;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$number.'', $nomenclatura); 
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$number.'', date("dmY"));
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$number.'', $_POST['user_id']);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$number.'', $this->input->cookie('clienteNombre'));
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$number.'', $product['product_ean']);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$number.'', $product['product_title']);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$number.'', $pedido);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$number.'', number_format($product['descuento_import'], 3, '.', ''));
        }

        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(10);         
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);         
        $objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('A1'), 'B1:I1');        

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');         
        // Forzamos a la descarga 
        $objWriter->save('uploads/pedidosExcel/'.$identificador_global.'.csv');

        $this->mod_cart_pedidos->delete_cart($_POST['user_id']);

        $CodIduser = base64_encode($_POST['user_id']);
        $urlEncode = urlencode($CodIduser);

        $ext['action']          = 'redirect';
        $ext['redirect_target'] = base_url().'gracias?pedido='.$urlEncode.'';
        $ext['redirect_delay']  = '500';
        ajax_response('success', '', '1', '1', $ext);
        
    }


    public function exportarPedidoStand()
    {   

        $listaProductos = $this->mod_cart_pedidos->get_pedidos_stand($_GET['id_stand']);
        
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);         
        $objPHPExcel->getActiveSheet()->setTitle('Transfer');         
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Nro de Transfer');         
        $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Fecha');         
        $objPHPExcel->getActiveSheet()->setCellValue('C1', 'ID Cliente');         
        $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Razon Social');         
        $objPHPExcel->getActiveSheet()->setCellValue('E1', 'EAN');         
        $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Descripcion');         
        $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Pedido');         
        $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Descuento');
         


        foreach ($listaProductos as $key => $product) {

            $number = $key+2;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$number.'', $product['nomenclatura']); 
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$number.'', date("dmY", $product['fecha_pedido']));
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$number.'', $product['id_user']);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$number.'', $product['razon_social']);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$number.'', $product['product_ean']);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$number.'', $product['product_title']);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$number.'', $product['stock_elgido']);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$number.'', number_format($product['descuento_import'], 3, '.', ''));
        }

        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(10);         
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);         
        $objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('A1'), 'B1:I1');        


        header('Content-Disposition: attachment;filename="IMP'.rand(99, 99999).'.csv"');
        header('Cache-Control: max-age=0');


        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');    
        $objWriter->setSheetIndex(0);   // Select which sheet.
        $objWriter->setDelimiter(';');  // Define delimiter
       ob_get_clean();
       $objWriter->save('php://output');
       ob_end_flush();


        //$ext['action']          = 'redirect';
        //$ext['redirect_target'] = base_url().'gracias?pedido='.$urlEncode.'';
        //$ext['redirect_delay']  = '500';
        //ajax_response('success', '', '1', '1', $ext);
        
    }

}
