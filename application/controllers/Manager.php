<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manager extends MX_Controller {

    function __construct()
    {
        parent::__construct();

        // Si no estamos accediendo a la página de login, controlamos que la sesion este activa.
        if (!empty($this->uri->segment(2)))
        	$this->control_library->session_check();
    }

	public function index()
	{
		$this->lang->load('login_lang', DEFAULT_LANGUAGE);

		// Si el usuario esta logeado lo mandamos al dashboard
		if ($this->session->userdata('logged_in') == 'TRUE')
			redirect(base_url().GESTORP_MANAGER.'/dashboard');

		// Sino le mostramos la pagina de acceso.
			show_page('login', FALSE, 'login');
	}

	public function dashboard()
	{
		$data['cache'] 		= $this->output->get_cache_status('nwg_page');
		$data['PAGE_TITLE']	= '¡ '.$this->lang->line('general_hello').' '.$this->session->userdata('user_name').'!';
		show_page('dashboard', $data);
	}

	public function pages($type = 'simples')
	{
		if (!check_permissions('gestorp', 'manage_pages')) redirect(base_url().'manager');
		$this->lang->load('pages_lang', $this->session->userdata('user_language'));

		$action 	= $this->input->get('action');
		$id_page 	= $this->input->get('id');

		switch ($action) {
			case 'edit':
				$data['page'] 			= $this->pages->get_page($id_page, TRUE);
				$data['versions']		= $this->pages->get_versions($id_page);

				$data['PAGE_TITLE']		= $this->lang->line('pages_edit_page');
				$data['PAGE_SUB_TITLE']	= $data['page']['page_title'];
				$data['PAGE_BUTTON']['link']	= '?action=list';
				$data['PAGE_BUTTON']['class']	= 'green';
				$data['PAGE_BUTTON']['icon']	= 'fa-arrow-left';
				$data['PAGE_BUTTON']['text']	= $this->lang->line('pages_back');
				show_page('pages/editor', $data);
				break;
			case 'new':
				if (!check_permissions('gestorp', 'manage_create_pages')) 
					redirect(base_url().'manager');
				$data['sections']		= $this->pages->get_sections();
				$data['layouts']		= $this->layouts->get_layouts();
				$data['preset']			= $this->input->get('preset');
				$data['parent']			= $this->input->get('parent');
				$data['PAGE_TITLE']		= $this->lang->line('pages_new_page');
				show_page('pages/new', $data);
				break;
			default:
				switch ($type) {
					case 'grouped':
						$this->pages_grouped();
						break;
					case 'external':
						$this->pages_external();
						break;
					default:
						$this->pages_simples();
						break;
				}
				break;
		}
	}
	private function pages_simples()
	{
        $conditions['is_section']       = FALSE;
        $conditions['is_external']      = FALSE;
        $conditions['id_parent_page']   = 0;
        $data['pages']	= $this->pages->get_pages($conditions, FALSE, FALSE, 500);
		
		$data['PAGE_TITLE']				= $this->lang->line('pages_pages_simples');
		
		if (check_permissions('gestorp', 'manage_create_pages')) 
		{
			$data['PAGE_BUTTON']['link']	= '?action=new&preset=simple';
			$data['PAGE_BUTTON']['class']	= 'green';
			$data['PAGE_BUTTON']['icon']	= 'fa-plus';
			$data['PAGE_BUTTON']['text']	= $this->lang->line('pages_new_page');
		}

		show_page('pages/simples', $data);
	}
	private function pages_grouped()
	{
		$data['sections']		= $this->pages->get_sections(TRUE);
		$data['PAGE_TITLE']		= $this->lang->line('pages_pages_grouped');
		
		if (check_permissions('gestorp', 'manage_create_pages')) 
		{
			$data['PAGE_BUTTON']['link']	= '?action=new&preset=grouped';
			$data['PAGE_BUTTON']['class']	= 'green';
			$data['PAGE_BUTTON']['icon']	= 'fa-plus';
			$data['PAGE_BUTTON']['text']	= $this->lang->line('pages_new_page');
		}

		show_page('pages/grouped', $data);
	}
	private function pages_external()
	{
        $conditions['is_section']       = FALSE;
        $conditions['is_external']      = TRUE;
        $conditions['id_parent_page']   = 0;
        $data['pages']	= $this->pages->get_pages($conditions, FALSE, FALSE, 500);
		
		$data['PAGE_TITLE']				= $this->lang->line('pages_pages_external');
		
		if (check_permissions('gestorp', 'manage_create_pages')) 
		{
			$data['PAGE_BUTTON']['link']	= '?action=new&preset=external';
			$data['PAGE_BUTTON']['class']	= 'green';
			$data['PAGE_BUTTON']['icon']	= 'fa-plus';
			$data['PAGE_BUTTON']['text']	= $this->lang->line('pages_new_page');
		}

		show_page('pages/external', $data);
	}


	public function modules($module, $sub_route = FALSE)
	{
		$this->control_library->session_check();
		if (!in_array($module, get_modules())) redirect(base_url().'manager');
		if (!check_permissions('gestorp', 'manage_modules')) redirect(base_url().'manager');
		if (!check_permissions('modules', $module)) redirect(base_url().'manager'); // Se puede omitir, el primero control lo cubre.

		$module_full_route 	= func_get_args();
		
		if ($sub_route !== FALSE)
			$module_route 		= implode('/', $module_full_route);
		else
			$module_route 		= $module.'/module_managment';

		$data['PAGE_TITLE']	 = ucfirst(implode(' ', explode('_', $module)));
		$data['MODULE_BODY'] = Modules::run($module_route);

		show_page('modules', $data);
	}

	public function media()
	{
		$this->control_library->session_check();

		// Este control es porque el plugin para los uploads hace un request a la misma url donde se encuentra. Ahorramos algo de trafico.
		if ($this->input->is_ajax_request()) exit();

		if (!check_permissions('gestorp', 'manage_media')) redirect(base_url().'manager');
		$this->lang->load('media_lang', $this->session->userdata('user_language'));

		$action 	= $this->input->get('action');
		$id_media 	= $this->input->get('id');

		switch ($action) {
			case 'edit':
				$data['media'] 			= $this->media->get_file($id_media);
				$data['media_groups'] 	= $this->media->get_groups();

				$data['PAGE_TITLE']		= $this->lang->line('media_edit_item');
				$data['PAGE_BUTTON']['link']	= '?action=gallery';
				$data['PAGE_BUTTON']['class']	= 'green';
				$data['PAGE_BUTTON']['icon']	= 'fa-arrow-left';
				$data['PAGE_BUTTON']['text']	= $this->lang->line('media_back_gallery');
				show_page('media/editor', $data);
				break;
			case 'manage_groups':
				$data['media_groups'] 			= $this->media->get_groups();
				$data['PAGE_TITLE']				= $this->lang->line('media_manage_group');
				$data['PAGE_BUTTON']['link']	= 'javascript:;';
				$data['PAGE_BUTTON']['class']	= 'green btn-new-media-group';
				$data['PAGE_BUTTON']['icon']	= 'fa-plus';
				$data['PAGE_BUTTON']['text']	= $this->lang->line('media_new_group');
				show_page('media/manage_groups', $data);
				break;
			default:
				$data['PAGE_TITLE']		= $this->lang->line('media_media');
				$data['PAGE_BUTTON']['link']	= '?action=manage_groups';
				$data['PAGE_BUTTON']['class']	= 'green';
				$data['PAGE_BUTTON']['icon']	= 'fa-folder';
				$data['PAGE_BUTTON']['text']	= $this->lang->line('media_manage_group');
				show_page('media', $data);
				break;
		}
	}

	public function configuration()
	{
		$this->control_library->session_check();
		if (!check_permissions('gestorp', 'manage_configurations')) redirect(base_url().'manager');

		$data['PAGE_TITLE']		= $this->lang->line('general_configuration');
		show_page('configuration', $data);
	}

	public function users()
	{
		$this->control_library->session_check();
		if (!check_permissions('gestorp', 'manage_users')) redirect(base_url().'manager');
		$this->lang->load('users_lang', $this->session->userdata('user_language'));

		// Levantamos variables GET
		$action 	= $this->input->get('action');
		$id_user 	= $this->input->get('id');
		$page 	 	= (int)$this->input->get('pagina');
		$page 	 	= ($page > 0) ? $page - 1 : 0;


		switch ($action) {
			case 'edit':
				$data['user'] 			= $this->users->get_user($id_user);

				if (check_permissions('gestorp', 'manage_permissions'))
				$data['permissions'] 	=	$this->permissions->get_user_permissions($id_user);

				$data['PAGE_TITLE']		= $this->lang->line('users_edit_user');
				$data['PAGE_SUB_TITLE']	= $data['user']['user_name'];
				show_page('users/editor', $data);
				break;
			case 'new':
				$data['PAGE_TITLE']		= $this->lang->line('users_create_user');
				show_page('users/new', $data);
				break;
			default:
				$data['users'] = $this->users->get_users(FALSE, FALSE, $page, PAGINATION_USER_LIST);
				
				$data['pagination_current']		= $page;
				$data['pagination_total_items']	= @$data['users'][0]['total_results'];
				$data['pagination_per_page']	= PAGINATION_USER_LIST;

				$data['PAGE_TITLE']				= $this->lang->line('users_users');
				$data['PAGE_BUTTON']['link']	= '?action=new';
				$data['PAGE_BUTTON']['class']	= 'green';
				$data['PAGE_BUTTON']['icon']	= 'fa-plus';
				$data['PAGE_BUTTON']['text']	= $this->lang->line('users_new_user');

				show_page('users', $data);
				break;
		}
	}

	public function account()
	{
		$this->control_library->session_check();
		$this->lang->load('users_lang', $this->session->userdata('user_language'));
		$id_user = $this->session->userdata('id_user');

		$data['PAGE_TITLE']	= $this->lang->line('general_account_configuration');
		$data['user'] 		= $this->users->get_user($id_user);

		show_page('users/editor', $data);
	}

	public function system()
	{
		$this->control_library->session_check();
		$data['PAGE_TITLE']		= $this->lang->line('general_system');
		show_page('system', $data);
	}

	// Pantallas de Edicion de paginas

		// Es la pantalla de editor de paginas.
		public function page_edit()
		{
			$this->lang->load('pages_lang', $this->session->userdata('user_language'));

			// Controlamos que tenga sesion activa.
			$this->control_library->session_check();

			// Levantamos parametros
			$id_page 		= $this->input->get('page');
			$version 		= $this->input->get('version');
			
			// Controlamos que tenga permiso de edicio para paginas, y para esta pagina.
	        if (!check_permissions('gestorp', 'manage_pages')) redirect(base_url().'manager');
	        if (!check_permissions('pages', $id_page)) redirect(base_url().'manager');

	        // Levantamos datos para completar la solicitud
			$data['available_languages']		= $this->config->item('available_languages');
			$data['avalilable_components']		= $this->components->get_nested_avalilable_components();
			
			if (in_array($this->input->get('lang'), $data['available_languages']))
				$data['lang'] = $this->input->get('lang');
			else
				$data['lang'] = $data['available_languages'][0];

			$data['id_page'] 	= $id_page;
			$data['version'] 	= $version;
			$data['version_data'] 	= $this->pages->get_version(FALSE, $id_page, $version);
			$data['page'] 		= $this->pages->get_page($id_page);

			if ($data['page'] == FALSE OR $data['version_data'] == FALSE)
			{
				show_error("La pagina solicitada no se encuentra disponible.", 500, 'Error de GestorP');
			}

			show_page('editor', $data, 'editor');
		}

		// Es el frame editor de contenido, se que se muestra en la pantalla de editor de paginas.
		public function page_editor()
		{
			// Controlamos que tenga sesion activa.
			$this->control_library->session_check();

			// Levantamos parametros
			$id_page 		= $this->input->get('id_page');
			$version 		= $this->input->get('version');
			$lang 			= $this->input->get('lang');

			// Controlamos que tenga permiso de edicio para paginas, y para esta pagina.
	        if (!check_permissions('gestorp', 'manage_pages')) redirect(base_url().'manager');
	        if (!check_permissions('pages', $id_page)) redirect(base_url().'manager');

			// Render page editor, renderiza una pagina de forma similar a la publica, 
			// pero encapsula los componentes en elementos draggeables.		
			render_page($id_page, $lang, $version , $edit = TRUE, $return = FALSE);
		}

		// Muestra un preview de la version
		public function page_preview()
		{
			// Controlamos que tenga sesion activa.
			$this->control_library->session_check();

			// Levantamos parametros
			$id_page 		= $this->input->get('page');
			$version 		= $this->input->get('version');
			$lang 			= $this->input->get('lang');
			
			// Controlamos que tenga permiso de edicio para paginas, y para esta pagina.
	        if (!check_permissions('gestorp', 'manage_pages')) redirect(base_url().'manager');
	        if (!check_permissions('pages', $id_page)) redirect(base_url().'manager');
	        
			render_page($id_page, $lang, $version, $edit = FALSE, $return = FALSE);
		}

		public function backups()
		{
	        $files = glob(FCPATH . 'uploads/backups/*.zip' );

	        // Limpiamos las rutas, porque glob nos da la ruta completa del directorio.
	        foreach ($files as $key => $file) 
	        {
	            $file = explode('/', $file);
	            if (isset($file[1]))
	                $files[$key]  = $file[count($file) - 1];   // tomamos solo el nombre del archivo.
	        }

			$data['files'] = $files;
			show_page('system/backups', $data);
		}

		// Genera un backup de toda la DB.
		public function generate_backup()
		{
	       $this->load->dbutil();   
	       $backup = $this->dbutil->backup();  
	       $this->load->helper('file');
	       
	       if (!file_exists(FCPATH . 'uploads/backups/')) {
	       		mkdir(FCPATH . 'uploads/backups/');
	       }

	       $r = write_file(FCPATH . 'uploads/backups/site-backup-'.date('Ymd').'.zip', $backup); 

	       if ($r) 
	    		echo "Se generó el backup correctamente.";
	       else
	    		echo "No se pudo guardar el backup.";
		}

		public function testc()
		{
			$r = $this->output->clean_cache('nwg_page');
			debugger($r);
		}

}
